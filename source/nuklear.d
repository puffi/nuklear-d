/*
 Nuklear - v1.156 - public domain
 no warrenty implied; use at your own risk.
 authored from 2015-2016 by Micha Mettke

ABOUT:
    This is a minimal state immediate mode graphical user interface single header
    toolkit written in ANSI C and licensed under public domain.
    It was designed as a simple embeddable user interface for application and does
    not have any dependencies, a default renderbackend or OS window and input handling
    but instead provides a very modular library approach by using simple input state
    for input and draw commands describing primitive shapes as output.
    So instead of providing a layered library that tries to abstract over a number
    of platform and render backends it only focuses on the actual UI.

VALUES:
    - Immediate mode graphical user interface toolkit
    - Single header library
    - Written in C89 (A.K.A. ANSI C or ISO C90)
    - Small codebase (~17kLOC)
    - Focus on portability, efficiency and simplicity
    - No dependencies (not even the standard library if not wanted)
    - Fully skinnable and customizable
    - Low memory footprint with total memory control if needed or wanted
    - UTF-8 support
    - No global or hidden state
    - Customizable library modules (you can compile and use only what you need)
    - Optional font baker and vertex buffer output

USAGE:
    This library is self contained in one single header file and can be used either
    in header only mode or in implementation mode. The header only mode is used
    by default when included and allows including this header in other headers
    and does not contain the actual implementation.

    The implementation mode requires to define  the preprocessor macro
    NK_IMPLEMENTATION in *one* .c/.cpp file before #includeing this file, e.g.:

        enum NK_IMPLEMENTATION
        #include "nuklear.h"

    Also optionally define the symbols listed in the section "OPTIONAL DEFINES"
    below in header and implementation mode if you want to use additional functionality
    or need more control over the library.
    IMPORTANT:  Every time you include "nuklear.h" you have to define the same flags.
                This is very important not doing it either leads to compiler errors
                or even worse stack corruptions.

FEATURES:
    - Absolutely no platform dependend code
    - Memory management control ranging from/to
        - Ease of use by allocating everything from the standard library
        - Control every byte of memory inside the library
    - Font handling control ranging from/to
        - Use your own font implementation for everything
        - Use this libraries internal font baking and handling API
    - Drawing output control ranging from/to
        - Simple shapes for more high level APIs which already have drawing capabilities
        - Hardware accessible anti-aliased vertex buffer output
    - Customizable colors and properties ranging from/to
        - Simple changes to color by filling a simple color table
        - Complete control with ability to use skinning to decorate widgets
    - Bendable UI library with widget ranging from/to
        - Basic widgets like buttons, checkboxes, slider, ...
        - Advanced widget like abstract comboboxes, contextual menus,...
    - Compile time configuration to only compile what you need
        - Subset which can be used if you do not want to link or use the standard library
    - Can be easily modified to only update on user input instead of frame updates

OPTIONAL DEFINES:
    NK_PRIVATE
        If defined declares all functions as static, so they can only be accessed
        for the file that creates the implementation

    NK_INCLUDE_FIXED_TYPES
        If defined it will include header <stdint.h> for fixed sized types
        otherwise nuklear tries to select the correct type. If that fails it will
        throw a compiler error and you have to select the correct types yourself.
        <!> If used needs to be defined for implementation and header <!>

    NK_INCLUDE_DEFAULT_ALLOCATOR
        if defined it will include header <stdlib.h> and provide additional functions
        to use this library without caring for memory allocation control and therefore
        ease memory management.
        <!> Adds the standard library with malloc and free so don't define if you
            don't want to link to the standard library <!>
        <!> If used needs to be defined for implementation and header <!>

    NK_INCLUDE_STANDARD_IO
        if defined it will include header <stdio.h> and provide
        additional functions depending on file loading.
        <!> Adds the standard library with fopen, fclose,... so don't define this
            if you don't want to link to the standard library <!>
        <!> If used needs to be defined for implementation and header <!>

    NK_INCLUDE_STANDARD_VARARGS
        if defined it will include header <stdarg.h> and provide
        additional functions depending on variable arguments
        <!> Adds the standard library with va_list and  so don't define this if
            you don't want to link to the standard library<!>
        <!> If used needs to be defined for implementation and header <!>

    NK_INCLUDE_VERTEX_BUFFER_OUTPUT
        Defining this adds a vertex draw command list backend to this
        library, which allows you to convert queue commands into vertex draw commands.
        This is mainly if you need a hardware accessible format for OpenGL, DirectX,
        Vulkan, Metal,...
        <!> If used needs to be defined for implementation and header <!>

    NK_INCLUDE_FONT_BAKING
        Defining this adds the `stb_truetype` and `stb_rect_pack` implementation
        to this library and provides font baking and rendering.
        If you already have font handling or do not want to use this font handler
        you don't have to define it.
        <!> If used needs to be defined for implementation and header <!>

    NK_INCLUDE_DEFAULT_FONT
        Defining this adds the default font: ProggyClean.ttf into this library
        which can be loaded into a font atlas and allows using this library without
        having a truetype font
        <!> Enabling this adds ~12kb to global stack memory <!>
        <!> If used needs to be defined for implementation and header <!>

    NK_INCLUDE_COMMAND_USERDATA
        Defining this adds a userdata pointer into each command. Can be useful for
        example if you want to provide custom shaders depending on the used widget.
        Can be combined with the style structures.
        <!> If used needs to be defined for implementation and header <!>

    NK_BUTTON_TRIGGER_ON_RELEASE
        Different platforms require button clicks occuring either on buttons being
        pressed (up to down) or released (down to up).
        By default this library will react on buttons being pressed, but if you
        define this it will only trigger if a button is released.
        <!> If used it is only required to be defined for the implementation part <!>

    NK_ASSERT
        If you don't define this, nuklear will use <assert.h> with assert().
        <!> Adds the standard library so define to nothing of not wanted <!>
        <!> If used needs to be defined for implementation and header <!>

    NK_BUFFER_DEFAULT_INITIAL_SIZE
        Initial buffer size allocated by all buffers while using the default allocator
        functions included by defining NK_INCLUDE_DEFAULT_ALLOCATOR. If you don't
        want to allocate the default 4k memory then redefine it.
        <!> If used needs to be defined for implementation and header <!>

    NK_MAX_NUMBER_BUFFER
        Maximum buffer size for the conversion buffer between float and string
        Under normal circumstances this should be more than sufficient.
        <!> If used needs to be defined for implementation and header <!>

    NK_INPUT_MAX
        Defines the max number of bytes which can be added as text input in one frame.
        Under normal circumstances this should be more than sufficient.
        <!> If used it is only required to be defined for the implementation part <!>

    NK_MEMSET
        You can define this to 'memset' or your own memset implementation
        replacement. If not nuklear will use its own version.
        <!> If used it is only required to be defined for the implementation part <!>

    NK_MEMCOPY
        You can define this to 'memcpy' or your own memcpy implementation
        replacement. If not nuklear will use its own version.
        <!> If used it is only required to be defined for the implementation part <!>

    NK_SQRT
        You can define this to 'sqrt' or your own sqrt implementation
        replacement. If not nuklear will use its own slow and not highly
        accurate version.
        <!> If used it is only required to be defined for the implementation part <!>

    NK_SIN
        You can define this to 'sinf' or your own sine implementation
        replacement. If not nuklear will use its own approximation implementation.
        <!> If used it is only required to be defined for the implementation part <!>

    NK_COS
        You can define this to 'cosf' or your own cosine implementation
        replacement. If not nuklear will use its own approximation implementation.
        <!> If used it only needs to be define for the implementation not header  <!>
        <!> If used it is only required to be defined for the implementation part <!>

    NK_STRTOD
        You can define this to `strtod` or your own string to double conversion
        implementation replacement. If not defined nuklear will use its own
        imprecise and possibly unsafe version (does not handle nan or infinity!).
        <!> If used it is only required to be defined for the implementation part <!>

    NK_DTOA
        You can define this to `dtoa` or your own double to string conversion
        implementation replacement. If not defined nuklear will use its own
        imprecise and possibly unsafe version (does not handle nan or infinity!).
        <!> If used it is only required to be defined for the implementation part <!>

    NK_VSNPRINTF
        If you define `NK_INCLUDE_STANDARD_VARARGS` as well as `NK_INCLUDE_STANDARD_IO`
        and want to be safe define this to `vsnprintf` on compilers supporting
        later versions of C or C++. By default nuklear will check for your stdlib version
        in C as well as compiler version in C++. if `vsnprintf` is available
        it will define it to `vsnprintf` directly. If not defined and if you have
        older versions of C or C++ it will be defined to `vsprintf` which is unsafe.
        <!> If used it is only required to be defined for the implementation part <!>

    NK_BYTE
    NK_INT16
    NK_UINT16
    NK_INT32
    NK_UINT32
    NK_SIZE_TYPE
    NK_POINTER_TYPE
        If you compile without NK_USE_FIXED_TYPE then a number of standard types
        will be selected and compile time validated. If they are incorrect you can
        define the correct types by overloading these type defines.

    NK_ZERO_COMMAND_MEMORY
        Defining this will zero out memory for each drawing command added to a
        drawing queue (inside nk_command_buffer_push). This can be used to
        implement fast check (using memcmp) that command buffers are equal and
        avoid drawing frames when nothing on screen has changed since previous
        frame.

CREDITS:
    Developed by Micha Mettke and every direct or indirect contributor to the GitHub.

    Embeds stb_texedit, stb_truetype and stb_rectpack by Sean Barret (public domain)
    Embeds ProggyClean.ttf font by Tristan Grimmer (MIT license).

    Big thank you to Omar Cornut (ocornut@github) for his imgui library and
    giving me the inspiration for this library, Casey Muratori for handmade hero
    and his original immediate mode graphical user interface idea and Sean
    Barret for his amazing single header libraries which restored my faith
    in libraries and brought me to create some of my own.

LICENSE:
    This software is dual-licensed to the public domain and under the following
    license: you are granted a perpetual, irrevocable license to copy, modify,
    publish and distribute this file as you see fit.
*/

module nuklear;

extern(C):
/*
 * ==============================================================
 *
 *                          CONSTANTS
 *
 * ===============================================================
 */
enum NK_UNDEFINED = (-1.0f);
enum NK_UTF_INVALID = 0xFFFD; /* internal invalid utf8 rune */
enum NK_UTF_SIZE = 4; /* describes the number of bytes a glyph consists of*/
enum NK_INPUT_MAX = 16;
enum NK_MAX_NUMBER_BUFFER = 64;
enum NK_SCROLLBAR_HIDING_TIMEOUT = 4.0f;

/*
 * ==============================================================
 *
 *                          HELPER
 *
 * ===============================================================
 */

auto NK_FLAG(T)(T x) { return (1 << (x)); }
// enum NK_STRINGIFY(x) #x
// enum NK_MACRO_STRINGIFY(x) NK_STRINGIFY(x)
// enum NK_STRING_JOIN_IMMEDIATE(arg1, arg2) arg1 ## arg2
// enum NK_STRING_JOIN_DELAY(arg1, arg2) NK_STRING_JOIN_IMMEDIATE(arg1, arg2)
// enum NK_STRING_JOIN(arg1, arg2) NK_STRING_JOIN_DELAY(arg1, arg2)

// #ifdef _MSC_VER
// enum NK_UNIQUE_NAME(name) NK_STRING_JOIN(name,__COUNTER__)
// #else
// enum NK_UNIQUE_NAME(name) NK_STRING_JOIN(name,__LINE__)
// #endif

// #ifndef NK_STATIC_ASSERT
// enum NK_STATIC_ASSERT(exp) typedef char NK_UNIQUE_NAME(_dummy_array)[(exp)?1:-1]
// #endif

// #ifndef NK_FILE_LINE
// #ifdef _MSC_VER
// enum NK_FILE_LINE __FILE__ ":" NK_MACRO_STRINGIFY(__COUNTER__)
// #else
// enum NK_FILE_LINE __FILE__ ":" NK_MACRO_STRINGIFY(__LINE__)
// #endif
// #endif

template NK_FILE_LINE(string file = __FILE__, string line = __LINE__)
{
	enum NK_FILE_LINE = file ~ ":" ~ line;
}

/*
 * ===============================================================
 *
 *                          BASIC
 *
 * ===============================================================
 */

import core.stdc.stdint;

alias NK_INT8 = byte;
alias NK_UINT8 = ubyte;
alias NK_INT16 = short;
alias NK_UINT16 = ushort;
alias NK_INT32 = int;
alias NK_UINT32 = uint;
alias NK_SIZE_TYPE = uintptr_t;
alias NK_POINTER_TYPE = uintptr_t;

alias NK_INT8 nk_char;
alias NK_UINT8 nk_uchar;
alias NK_UINT8 nk_byte;
alias NK_INT16 nk_short;
alias NK_UINT16 nk_ushort;
alias NK_INT32 nk_int;
alias NK_UINT32 nk_uint;
alias NK_SIZE_TYPE nk_size;
alias NK_POINTER_TYPE nk_ptr;

alias nk_uint nk_hash;
alias nk_uint nk_flags;
alias nk_uint nk_rune;

/* ============================================================================
 *
 *                                  API
 *
 * =========================================================================== */

enum {nk_false, nk_true};
struct Color {nk_byte r,g,b,a;}
struct Colorf {float r,g,b,a;}
struct Vec2 {float x,y;}
struct Vec2i {short x, y;}
struct Rect {float x,y,w,h;}
struct Recti {short x,y,w,h;}
alias Glyph = char[NK_UTF_SIZE];
union HandleType { void *ptr; int id; }
alias Handle = HandleType;
struct Image {Handle handle;ushort w,h;ushort[4] region;};
struct Cursor {Image img; Vec2 size, offset;};
struct Scroll {ushort x, y;};
enum Heading {NK_UP, NK_RIGHT, NK_DOWN, NK_LEFT};

enum ButtonBehavior {NK_BUTTON_DEFAULT, NK_BUTTON_REPEATER};
enum Modify          {NK_FIXED = nk_false, NK_MODIFIABLE = nk_true};
enum Orientation     {NK_VERTICAL, NK_HORIZONTAL};
enum CollapseStates {NK_MINIMIZED = nk_false, NK_MAXIMIZED = nk_true};
enum ShowStates     {NK_HIDDEN = nk_false, NK_SHOWN = nk_true};
enum ChartType      {NK_CHART_LINES, NK_CHART_COLUMN, NK_CHART_MAX};
enum ChartEvent     {NK_CHART_HOVERING = 0x01, NK_CHART_CLICKED = 0x02};
enum ColorFormat    {NK_RGB, NK_RGBA};
enum PopupType      {NK_POPUP_STATIC, NK_POPUP_DYNAMIC};
enum LayoutFormat   {NK_DYNAMIC, NK_STATIC};
enum TreeType       {NK_TREE_NODE, NK_TREE_TAB};
enum AntiAliasing   {NK_ANTI_ALIASING_OFF, NK_ANTI_ALIASING_ON};

alias nk_plugin_alloc = void* function(Handle, void* old, nk_size);
alias nk_plugin_free = void function(Handle, void* old);
alias nk_plugin_filter = int function(const(TextEdit)*, nk_rune unicode);
alias nk_plugin_paste = void function(Handle, TextEdit*);
alias nk_plugin_copy = void function(Handle, const(char)*, int len);

struct Allocator {
    Handle userdata;
    nk_plugin_alloc alloc;
    nk_plugin_free free;
};

struct DrawNullTexture {
    Handle texture;/* texture handle to a texture with a white pixel */
    Vec2 uv; /* coordinates to a white pixel in the texture  */
};
struct ConvertConfig {
    float global_alpha; /* global alpha value */
    AntiAliasing line_AA; /* line anti-aliasing flag can be turned off if you are tight on memory */
    AntiAliasing shape_AA; /* shape anti-aliasing flag can be turned off if you are tight on memory */
    uint circle_segment_count; /* number of segments used for circles: default to 22 */
    uint arc_segment_count; /* number of segments used for arcs: default to 22 */
    uint curve_segment_count; /* number of segments used for curves: default to 22 */
    DrawNullTexture null_; /* handle to texture with a white pixel for shape drawing */
    const(DrawVertexLayoutElement) *vertex_layout; /* describes the vertex output format and packing */
    nk_size vertex_size; /* sizeof one vertex for vertex packing */
    nk_size vertex_alignment; /* vertex alignment: Can be optained by NK_ALIGNOF */
};

enum SymbolType {
    NK_SYMBOL_NONE,
    NK_SYMBOL_X,
    NK_SYMBOL_UNDERSCORE,
    NK_SYMBOL_CIRCLE_SOLID,
    NK_SYMBOL_CIRCLE_OUTLINE,
    NK_SYMBOL_RECT_SOLID,
    NK_SYMBOL_RECT_OUTLINE,
    NK_SYMBOL_TRIANGLE_UP,
    NK_SYMBOL_TRIANGLE_DOWN,
    NK_SYMBOL_TRIANGLE_LEFT,
    NK_SYMBOL_TRIANGLE_RIGHT,
    NK_SYMBOL_PLUS,
    NK_SYMBOL_MINUS,
    NK_SYMBOL_MAX
};

enum Keys {
    NK_KEY_NONE,
    NK_KEY_SHIFT,
    NK_KEY_CTRL,
    NK_KEY_DEL,
    NK_KEY_ENTER,
    NK_KEY_TAB,
    NK_KEY_BACKSPACE,
    NK_KEY_COPY,
    NK_KEY_CUT,
    NK_KEY_PASTE,
    NK_KEY_UP,
    NK_KEY_DOWN,
    NK_KEY_LEFT,
    NK_KEY_RIGHT,

    /* Shortcuts: text field */
    NK_KEY_TEXT_INSERT_MODE,
    NK_KEY_TEXT_REPLACE_MODE,
    NK_KEY_TEXT_RESET_MODE,
    NK_KEY_TEXT_LINE_START,
    NK_KEY_TEXT_LINE_END,
    NK_KEY_TEXT_START,
    NK_KEY_TEXT_END,
    NK_KEY_TEXT_UNDO,
    NK_KEY_TEXT_REDO,
    NK_KEY_TEXT_WORD_LEFT,
    NK_KEY_TEXT_WORD_RIGHT,

    /* Shortcuts: scrollbar */
    NK_KEY_SCROLL_START,
    NK_KEY_SCROLL_END,
    NK_KEY_SCROLL_DOWN,
    NK_KEY_SCROLL_UP,

    NK_KEY_MAX
};

enum Buttons {
    NK_BUTTON_LEFT,
    NK_BUTTON_MIDDLE,
    NK_BUTTON_RIGHT,
    NK_BUTTON_MAX
};

enum StyleColors {
    NK_COLOR_TEXT,
    NK_COLOR_WINDOW,
    NK_COLOR_HEADER,
    NK_COLOR_BORDER,
    NK_COLOR_BUTTON,
    NK_COLOR_BUTTON_HOVER,
    NK_COLOR_BUTTON_ACTIVE,
    NK_COLOR_TOGGLE,
    NK_COLOR_TOGGLE_HOVER,
    NK_COLOR_TOGGLE_CURSOR,
    NK_COLOR_SELECT,
    NK_COLOR_SELECT_ACTIVE,
    NK_COLOR_SLIDER,
    NK_COLOR_SLIDER_CURSOR,
    NK_COLOR_SLIDER_CURSOR_HOVER,
    NK_COLOR_SLIDER_CURSOR_ACTIVE,
    NK_COLOR_PROPERTY,
    NK_COLOR_EDIT,
    NK_COLOR_EDIT_CURSOR,
    NK_COLOR_COMBO,
    NK_COLOR_CHART,
    NK_COLOR_CHART_COLOR,
    NK_COLOR_CHART_COLOR_HIGHLIGHT,
    NK_COLOR_SCROLLBAR,
    NK_COLOR_SCROLLBAR_CURSOR,
    NK_COLOR_SCROLLBAR_CURSOR_HOVER,
    NK_COLOR_SCROLLBAR_CURSOR_ACTIVE,
    NK_COLOR_TAB_HEADER,
    NK_COLOR_COUNT
};

enum StyleCursor {
    NK_CURSOR_ARROW,
    NK_CURSOR_TEXT,
    NK_CURSOR_MOVE,
    NK_CURSOR_RESIZE_VERTICAL,
    NK_CURSOR_RESIZE_HORIZONTAL,
    NK_CURSOR_RESIZE_TOP_LEFT_DOWN_RIGHT,
    NK_CURSOR_RESIZE_TOP_RIGHT_DOWN_LEFT,
    NK_CURSOR_COUNT
};

enum WidgetLayoutStates {
    NK_WIDGET_INVALID, /* The widget cannot be seen and is completely out of view */
    NK_WIDGET_VALID, /* The widget is completely inside the window and can be updated and drawn */
    NK_WIDGET_ROM /* The widget is partially visible and cannot be updated */
};

/* widget states */
enum WidgetStates {
    NK_WIDGET_STATE_MODIFIED    = NK_FLAG(1),
    NK_WIDGET_STATE_INACTIVE    = NK_FLAG(2), /* widget is neither active nor hovered */
    NK_WIDGET_STATE_ENTERED     = NK_FLAG(3), /* widget has been hovered on the current frame */
    NK_WIDGET_STATE_HOVER       = NK_FLAG(4), /* widget is being hovered */
    NK_WIDGET_STATE_ACTIVED     = NK_FLAG(5),/* widget is currently activated */
    NK_WIDGET_STATE_LEFT        = NK_FLAG(6), /* widget is from this frame on not hovered anymore */
    NK_WIDGET_STATE_HOVERED     = NK_WIDGET_STATE_HOVER|NK_WIDGET_STATE_MODIFIED, /* widget is being hovered */
    NK_WIDGET_STATE_ACTIVE      = NK_WIDGET_STATE_ACTIVED|NK_WIDGET_STATE_MODIFIED /* widget is currently activated */
};

/* text alignment */
enum TextAlign {
    NK_TEXT_ALIGN_LEFT        = 0x01,
    NK_TEXT_ALIGN_CENTERED    = 0x02,
    NK_TEXT_ALIGN_RIGHT       = 0x04,
    NK_TEXT_ALIGN_TOP         = 0x08,
    NK_TEXT_ALIGN_MIDDLE      = 0x10,
    NK_TEXT_ALIGN_BOTTOM      = 0x20
};
enum TextAlignment {
    NK_TEXT_LEFT        = TextAlign.NK_TEXT_ALIGN_MIDDLE|TextAlign.NK_TEXT_ALIGN_LEFT,
    NK_TEXT_CENTERED    = TextAlign.NK_TEXT_ALIGN_MIDDLE|TextAlign.NK_TEXT_ALIGN_CENTERED,
    NK_TEXT_RIGHT       = TextAlign.NK_TEXT_ALIGN_MIDDLE|TextAlign.NK_TEXT_ALIGN_RIGHT
};

/* edit flags */
enum EditFlags {
    NK_EDIT_DEFAULT                 = 0,
    NK_EDIT_READ_ONLY               = NK_FLAG(0),
    NK_EDIT_AUTO_SELECT             = NK_FLAG(1),
    NK_EDIT_SIG_ENTER               = NK_FLAG(2),
    NK_EDIT_ALLOW_TAB               = NK_FLAG(3),
    NK_EDIT_NO_CURSOR               = NK_FLAG(4),
    NK_EDIT_SELECTABLE              = NK_FLAG(5),
    NK_EDIT_CLIPBOARD               = NK_FLAG(6),
    NK_EDIT_CTRL_ENTER_NEWLINE      = NK_FLAG(7),
    NK_EDIT_NO_HORIZONTAL_SCROLL    = NK_FLAG(8),
    NK_EDIT_ALWAYS_INSERT_MODE      = NK_FLAG(9),
    NK_EDIT_MULTILINE               = NK_FLAG(11),
    NK_EDIT_GOTO_END_ON_ACTIVATE    = NK_FLAG(12)
};
enum EditTypes {
    NK_EDIT_SIMPLE  = EditFlags.NK_EDIT_ALWAYS_INSERT_MODE,
    NK_EDIT_FIELD   = cast(EditFlags)NK_EDIT_SIMPLE|EditFlags.NK_EDIT_SELECTABLE|EditFlags.NK_EDIT_CLIPBOARD,
    NK_EDIT_BOX     = EditFlags.NK_EDIT_ALWAYS_INSERT_MODE| EditFlags.NK_EDIT_SELECTABLE| EditFlags.NK_EDIT_MULTILINE|EditFlags.NK_EDIT_ALLOW_TAB|EditFlags.NK_EDIT_CLIPBOARD,
    NK_EDIT_EDITOR  = EditFlags.NK_EDIT_SELECTABLE|EditFlags.NK_EDIT_MULTILINE|EditFlags.NK_EDIT_ALLOW_TAB| EditFlags.NK_EDIT_CLIPBOARD
};
enum EditEvents {
    NK_EDIT_ACTIVE      = NK_FLAG(0), /* edit widget is currently being modified */
    NK_EDIT_INACTIVE    = NK_FLAG(1), /* edit widget is not active and is not being modified */
    NK_EDIT_ACTIVATED   = NK_FLAG(2), /* edit widget went from state inactive to state active */
    NK_EDIT_DEACTIVATED = NK_FLAG(3), /* edit widget went from state active to state inactive */
    NK_EDIT_COMMITED    = NK_FLAG(4) /* edit widget has received an enter and lost focus */
};

enum PanelFlags {
    NK_WINDOW_BORDER            = NK_FLAG(0), /* Draws a border around the window to visually separate the window * from the background */
    NK_WINDOW_MOVABLE           = NK_FLAG(1), /* The movable flag indicates that a window can be moved by user input or * by dragging the window header */
    NK_WINDOW_SCALABLE          = NK_FLAG(2), /* The scalable flag indicates that a window can be scaled by user input * by dragging a scaler icon at the button of the window */
    NK_WINDOW_CLOSABLE          = NK_FLAG(3), /* adds a closable icon into the header */
    NK_WINDOW_MINIMIZABLE       = NK_FLAG(4), /* adds a minimize icon into the header */
    NK_WINDOW_NO_SCROLLBAR      = NK_FLAG(5), /* Removes the scrollbar from the window */
    NK_WINDOW_TITLE             = NK_FLAG(6), /* Forces a header at the top at the window showing the title */
    NK_WINDOW_SCROLL_AUTO_HIDE  = NK_FLAG(7), /* Automatically hides the window scrollbar if no user interaction */
    NK_WINDOW_BACKGROUND        = NK_FLAG(8) /* Always keep window in the background */
};

/* context */
int                      nk_init_default(Context*, const(UserFont)*);
int                      nk_init_fixed(Context*, void *memory, nk_size size, const(UserFont)*);
int                      nk_init_custom(Context*, Buffer *cmds, Buffer *pool, const(UserFont)*);
int                      nk_init(Context*, Allocator*, const(UserFont)*);
void                     nk_clear(Context*);
void                     nk_free(Context*);
void                     nk_set_user_data(Context*, Handle handle);

/* window */
int                      nk_begin(Context*, Panel*, const(char)* title, Rect bounds, nk_flags flags);
int                      nk_begin_titled(Context*, Panel*, const(char)* name, const(char)* title, Rect bounds, nk_flags flags);
void                     nk_end(Context*);

Window*        nk_window_find(Context *ctx, const(char)* name);
Rect           nk_window_get_bounds(const(Context)*);
Vec2           nk_window_get_position(const(Context)*);
Vec2           nk_window_get_size(const(Context)*);
float                    nk_window_get_width(const(Context)*);
float                    nk_window_get_height(const(Context)*);
Panel*         nk_window_get_panel(Context*);
Rect           nk_window_get_content_region(Context*);
Vec2           nk_window_get_content_region_min(Context*);
Vec2           nk_window_get_content_region_max(Context*);
Vec2           nk_window_get_content_region_size(Context*);
CommandBuffer* nk_window_get_canvas(Context*);

int                      nk_window_has_focus(const(Context)*);
int                      nk_window_is_collapsed(Context*, const(char)*);
int                      nk_window_is_closed(Context*, const(char)*);
int                      nk_window_is_hidden(Context*, const(char)*);
int                      nk_window_is_active(Context*, const(char)*);
int                      nk_window_is_hovered(Context*);
int                      nk_window_is_any_hovered(Context*);
int                      nk_item_is_any_active(Context*);

void                     nk_window_set_bounds(Context*, Rect);
void                     nk_window_set_position(Context*, Vec2);
void                     nk_window_set_size(Context*, Vec2);
void                     nk_window_set_focus(Context*, const(char)* name);

void                     nk_window_close(Context *ctx, const(char)* name);
void                     nk_window_collapse(Context*, const(char)* name, CollapseStates);
void                     nk_window_collapse_if(Context*, const(char)* name, CollapseStates, int cond);
void                     nk_window_show(Context*, const(char)* name, ShowStates);
void                     nk_window_show_if(Context*, const(char)* name, ShowStates, int cond);

/* Layout */
void                     nk_layout_row_dynamic(Context*, float height, int cols);
void                     nk_layout_row_static(Context*, float height, int item_width, int cols);

void                     nk_layout_row_begin(Context*, LayoutFormat, float row_height, int cols);
void                     nk_layout_row_push(Context*, float value);
void                     nk_layout_row_end(Context*);
void                     nk_layout_row(Context*, LayoutFormat, float height, int cols, const(float)* ratio);

void                     nk_layout_space_begin(Context*, LayoutFormat, float height, int widget_count);
void                     nk_layout_space_push(Context*, Rect);
void                     nk_layout_space_end(Context*);

Rect           nk_layout_space_bounds(Context*);
Vec2           nk_layout_space_to_screen(Context*, Vec2);
Vec2           nk_layout_space_to_local(Context*, Vec2);
Rect           nk_layout_space_rect_to_screen(Context*, Rect);
Rect           nk_layout_space_rect_to_local(Context*, Rect);
float                    nk_layout_ratio_from_pixel(Context*, float pixel_width);

/* Layout: Group */
int                      nk_group_begin(Context*, Panel*, const(char)* title, nk_flags);
void                     nk_group_end(Context*);

/* Layout: Tree */ // TODO
// enum                         nk_tree_push(ctx, type, title, state) nk_tree_push_hashed(ctx, type, title, state, NK_FILE_LINE,nk_strlen(NK_FILE_LINE),__LINE__)
// enum                         nk_tree_push_id(ctx, type, title, state, id) nk_tree_push_hashed(ctx, type, title, state, NK_FILE_LINE,nk_strlen(NK_FILE_LINE),id)
int                      nk_tree_push_hashed(Context*, TreeType, const(char)* title, CollapseStates initial_state, const(char)* hash, int len,int seed);
// enum                         nk_tree_image_push(ctx, type, img, title, state) nk_tree_image_push_hashed(ctx, type, img, title, state, NK_FILE_LINE,nk_strlen(NK_FILE_LINE),__LINE__)
// enum                         nk_tree_image_push_id(ctx, type, img, title, state, id) nk_tree_image_push_hashed(ctx, type, img, title, state, NK_FILE_LINE,nk_strlen(NK_FILE_LINE),id)
int                      nk_tree_image_push_hashed(Context*, TreeType, Image, const(char)* title, CollapseStates initial_state, const(char)* hash, int len,int seed);
void                     nk_tree_pop(Context*);

/* Widgets */
void                     nk_text(Context*, const(char)*, int, nk_flags);
void                     nk_text_colored(Context*, const(char)*, int, nk_flags, Color);
void                     nk_text_wrap(Context*, const(char)*, int);
void                     nk_text_wrap_colored(Context*, const(char)*, int, Color);

void                     nk_label(Context*, const(char)*, nk_flags align_);
void                     nk_label_colored(Context*, const(char)*, nk_flags align_, Color);
void                     nk_label_wrap(Context*, const(char)*);
void                     nk_label_colored_wrap(Context*, const(char)*, Color);
void                     nk_image(Context*, Image);

void                     nk_labelf(Context*, nk_flags, const(char)*, ...);
void                     nk_labelf_colored(Context*, nk_flags align_, Color, const(char)*,...);
void                     nk_labelf_wrap(Context*, const(char)*,...);
void                     nk_labelf_colored_wrap(Context*, Color, const(char)*,...);

void                     nk_value_bool(Context*, const(char)* prefix, int);
void                     nk_value_int(Context*, const(char)* prefix, int);
void                     nk_value_uint(Context*, const(char)* prefix, uint);
void                     nk_value_float(Context*, const(char)* prefix, float);
void                     nk_value_color_byte(Context*, const(char)* prefix, Color);
void                     nk_value_color_float(Context*, const(char)* prefix, Color);
void                     nk_value_color_hex(Context*, const(char)* prefix, Color);

/* Widgets: Buttons */
int                      nk_button_text(Context*, const(char)* title, int len);
int                      nk_button_label(Context*, const(char)* title);
int                      nk_button_color(Context*, Color);
int                      nk_button_symbol(Context*, SymbolType);
int                      nk_button_image(Context*, Image img);
int                      nk_button_symbol_label(Context*, SymbolType, const(char)*, nk_flags text_alignment);
int                      nk_button_symbol_text(Context*, SymbolType, const(char)*, int, nk_flags alignment);
int                      nk_button_image_label(Context*, Image img, const(char)*, nk_flags text_alignment);
int                      nk_button_image_text(Context*, Image img, const(char)*, int, nk_flags alignment);

void                     nk_button_set_behavior(Context*, ButtonBehavior);
int                      nk_button_push_behavior(Context*, ButtonBehavior);
int                      nk_button_pop_behavior(Context*);

/* Widgets: Checkbox */
int                      nk_check_label(Context*, const(char)*, int active);
int                      nk_check_text(Context*, const(char)*, int,int active);
uint                 nk_check_flags_label(Context*, const(char)*, uint flags, uint value);
uint                 nk_check_flags_text(Context*, const(char)*, int, uint flags, uint value);
int                      nk_checkbox_label(Context*, const(char)*, int *active);
int                      nk_checkbox_text(Context*, const(char)*, int, int *active);
int                      nk_checkbox_flags_label(Context*, const(char)*, uint *flags, uint value);
int                      nk_checkbox_flags_text(Context*, const(char)*, int, uint *flags, uint value);

/* Widgets: Radio */
int                      nk_radio_label(Context*, const(char)*, int *active);
int                      nk_radio_text(Context*, const(char)*, int, int *active);
int                      nk_option_label(Context*, const(char)*, int active);
int                      nk_option_text(Context*, const(char)*, int, int active);

/* Widgets: Selectable */
int                      nk_selectable_label(Context*, const(char)*, nk_flags align_, int *value);
int                      nk_selectable_text(Context*, const(char)*, int, nk_flags align_, int *value);
int                      nk_selectable_image_label(Context*,Image,  const(char)*, nk_flags align_, int *value);
int                      nk_selectable_image_text(Context*,Image, const(char)*, int, nk_flags align_, int *value);

int                      nk_select_label(Context*, const(char)*, nk_flags align_, int value);
int                      nk_select_text(Context*, const(char)*, int, nk_flags align_, int value);
int                      nk_select_image_label(Context*, Image,const(char)*, nk_flags align_, int value);
int                      nk_select_image_text(Context*, Image,const(char)*, int, nk_flags align_, int value);

/* Widgets: Slider */
float                    nk_slide_float(Context*, float min, float val, float max, float step);
int                      nk_slide_int(Context*, int min, int val, int max, int step);
int                      nk_slider_float(Context*, float min, float *val, float max, float step);
int                      nk_slider_int(Context*, int min, int *val, int max, int step);

/* Widgets: Progressbar */
int                      nk_progress(Context*, nk_size *cur, nk_size max, int modifyable);
nk_size                  nk_prog(Context*, nk_size cur, nk_size max, int modifyable);

/* Widgets: Color picker */
Color          nk_color_picker(Context*, Color, ColorFormat);
int                      nk_color_pick(Context*, Color*, ColorFormat);

/* Widgets: Property */
void                     nk_property_int(Context*, const(char)* name, int min, int *val, int max, int step, float inc_per_pixel);
void                     nk_property_float(Context*, const(char)* name, float min, float *val, float max, float step, float inc_per_pixel);
void                     nk_property_double(Context*, const(char)* name, double min, double *val, double max, double step, float inc_per_pixel);
int                      nk_propertyi(Context*, const(char)* name, int min, int val, int max, int step, float inc_per_pixel);
float                    nk_propertyf(Context*, const(char)* name, float min, float val, float max, float step, float inc_per_pixel);
double                   nk_propertyd(Context*, const(char)* name, double min, double val, double max, double step, float inc_per_pixel);

/* Widgets: TextEdit */
nk_flags                 nk_edit_string(Context*, nk_flags, char *buffer, int *len, int max, nk_plugin_filter);
nk_flags                 nk_edit_buffer(Context*, nk_flags, TextEdit*, nk_plugin_filter);
nk_flags                 nk_edit_string_zero_terminated(Context*, nk_flags, char *buffer, int max, nk_plugin_filter);

/* Chart */
int                      nk_chart_begin(Context*, ChartType, int num, float min, float max);
int                      nk_chart_begin_colored(Context*, ChartType, Color, Color active, int num, float min, float max);
void                     nk_chart_add_slot(Context *ctx, const ChartType, int count, float min_value, float max_value);
void                     nk_chart_add_slot_colored(Context *ctx, const ChartType, Color, Color active, int count, float min_value, float max_value);
nk_flags                 nk_chart_push(Context*, float);
nk_flags                 nk_chart_push_slot(Context*, float, int);
void                     nk_chart_end(Context*);
void                     nk_plot(Context*, ChartType, const(float)* values, int count, int offset);
void                     nk_plot_function(Context*, ChartType, void *userdata, float function(void* user, int index) value_getter, int count, int offset);

/* Popups */
int                      nk_popup_begin(Context*, Panel*, PopupType, const(char)*, nk_flags, Rect bounds);
void                     nk_popup_close(Context*);
void                     nk_popup_end(Context*);

/* Combobox */
int                      nk_combo(Context*, const(char)** items, int count, int selected, int item_height, Vec2 size);
int                      nk_combo_separator(Context*, const(char)* items_separated_by_separator, int separator, int selected, int count, int item_height, Vec2 size);
int                      nk_combo_string(Context*, const(char)* items_separated_by_zeros, int selected, int count, int item_height, Vec2 size);
int                      nk_combo_callback(Context*, void function(void*, int, const(char)**) item_getter, void *userdata, int selected, int count, int item_height, Vec2 size);
void                     nk_combobox(Context*, const(char)** items, int count, int *selected, int item_height, Vec2 size);
void                     nk_combobox_string(Context*, const(char)* items_separated_by_zeros, int *selected, int count, int item_height, Vec2 size);
void                     nk_combobox_separator(Context*, const(char)* items_separated_by_separator, int separator,int *selected, int count, int item_height, Vec2 size);
void                     nk_combobox_callback(Context*, void function(void*, int, const(char)**) item_getter, void*, int *selected, int count, int item_height, Vec2 size);

/* Combobox: abstract */
int                      nk_combo_begin_text(Context*, Panel*, const(char)* selected, int, Vec2 size);
int                      nk_combo_begin_label(Context*, Panel*, const(char)* selected, Vec2 size);
int                      nk_combo_begin_color(Context*, Panel*, Color color, Vec2 size);
int                      nk_combo_begin_symbol(Context*, Panel*, SymbolType,  Vec2 size);
int                      nk_combo_begin_symbol_label(Context*, Panel*, const(char)* selected, SymbolType, Vec2 size);
int                      nk_combo_begin_symbol_text(Context*, Panel*, const(char)* selected, int, SymbolType, Vec2 size);
int                      nk_combo_begin_image(Context*, Panel*, Image img,  Vec2 size);
int                      nk_combo_begin_image_label(Context*, Panel*, const(char)* selected, Image, Vec2 size);
int                      nk_combo_begin_image_text(Context*, Panel*, const(char)* selected, int, Image, Vec2 size);
int                      nk_combo_item_label(Context*, const(char)*, nk_flags alignment);
int                      nk_combo_item_text(Context*, const(char)*,int, nk_flags alignment);
int                      nk_combo_item_image_label(Context*, Image, const(char)*, nk_flags alignment);
int                      nk_combo_item_image_text(Context*, Image, const(char)*, int,nk_flags alignment);
int                      nk_combo_item_symbol_label(Context*, SymbolType, const(char)*, nk_flags alignment);
int                      nk_combo_item_symbol_text(Context*, SymbolType, const(char)*, int, nk_flags alignment);
void                     nk_combo_close(Context*);
void                     nk_combo_end(Context*);

/* Contextual */
int                      nk_contextual_begin(Context*, Panel*, nk_flags, Vec2, Rect trigger_bounds);
int                      nk_contextual_item_text(Context*, const(char)*, int,nk_flags align_);
int                      nk_contextual_item_label(Context*, const(char)*, nk_flags align_);
int                      nk_contextual_item_image_label(Context*, Image, const(char)*, nk_flags alignment);
int                      nk_contextual_item_image_text(Context*, Image, const(char)*, int len, nk_flags alignment);
int                      nk_contextual_item_symbol_label(Context*, SymbolType, const(char)*, nk_flags alignment);
int                      nk_contextual_item_symbol_text(Context*, SymbolType, const(char)*, int, nk_flags alignment);
void                     nk_contextual_close(Context*);
void                     nk_contextual_end(Context*);

/* Tooltip */
void                     nk_tooltip(Context*, const(char)*);
int                      nk_tooltip_begin(Context*, Panel*, float width);
void                     nk_tooltip_end(Context*);

/* Menu */
void                     nk_menubar_begin(Context*);
void                     nk_menubar_end(Context*);

int                      nk_menu_begin_text(Context*, Panel*, const(char)* title, int title_len, nk_flags align_, Vec2 size);
int                      nk_menu_begin_label(Context*, Panel*, const(char)*, nk_flags align_, Vec2 size);
int                      nk_menu_begin_image(Context*, Panel*, const(char)*, Image, Vec2 size);
int                      nk_menu_begin_image_text(Context*, Panel*, const(char)*, int,nk_flags align_,Image, Vec2 size);
int                      nk_menu_begin_image_label(Context*, Panel*, const(char)*, nk_flags align_,Image, Vec2 size);
int                      nk_menu_begin_symbol(Context*, Panel*, const(char)*, SymbolType, Vec2 size);
int                      nk_menu_begin_symbol_text(Context*, Panel*, const(char)*, int,nk_flags align_,SymbolType, Vec2 size);
int                      nk_menu_begin_symbol_label(Context*, Panel*, const(char)*, nk_flags align_,SymbolType, Vec2 size);
int                      nk_menu_item_text(Context*, const(char)*, int,nk_flags align_);
int                      nk_menu_item_label(Context*, const(char)*, nk_flags alignment);
int                      nk_menu_item_image_label(Context*, Image, const(char)*, nk_flags alignment);
int                      nk_menu_item_image_text(Context*, Image, const(char)*, int len, nk_flags alignment);
int                      nk_menu_item_symbol_text(Context*, SymbolType, const(char)*, int, nk_flags alignment);
int                      nk_menu_item_symbol_label(Context*, SymbolType, const(char)*, nk_flags alignment);
void                     nk_menu_close(Context*);
void                     nk_menu_end(Context*);

/* Drawing*/ // TODO
// enum                                 nk_foreach(c, ctx) for((c)=nk__begin(ctx); (c)!=0; (c)=nk__next(ctx, c))
void                             nk_convert(Context*, Buffer *cmds, Buffer *vertices, Buffer *elements, const(ConvertConfig)*);
// enum                                 nk_draw_foreach(cmd,ctx, b) for((cmd)=nk__draw_begin(ctx, b); (cmd)!=0; (cmd)=nk__draw_next(cmd, b, ctx))
// enum                                 nk_draw_foreach_bounded(cmd,from,to) for((cmd)=(from); (cmd) && (to) && (cmd)>=to; --(cmd))
const(DrawCommand)*    nk__draw_begin(const(Context)*, const(Buffer)*);
const(DrawCommand)*    nk__draw_end(const(Context)*, const(Buffer)*);
const(DrawCommand)*    nk__draw_next(const(DrawCommand)*, const(Buffer)*, const(Context)*);

/* User Input */
void                     nk_input_begin(Context*);
void                     nk_input_motion(Context*, int x, int y);
void                     nk_input_key(Context*, Keys, int down);
void                     nk_input_button(Context*, Buttons, int x, int y, int down);
void                     nk_input_scroll(Context*, float y);
void                     nk_input_char(Context*, char);
void                     nk_input_glyph(Context*, const Glyph);
void                     nk_input_unicode(Context*, nk_rune);
void                     nk_input_end(Context*);

/* Style */
void                     nk_style_default(Context*);
void                     nk_style_from_table(Context*, const Color*);
void                     nk_style_load_cursor(Context*, StyleCursor, const Cursor*);
void                     nk_style_load_all_cursors(Context*, Cursor*);
const(char)*              nk_style_get_color_by_name(StyleColors);
void                     nk_style_set_font(Context*, const(UserFont)*);
int                      nk_style_set_cursor(Context*, StyleCursor);
void                     nk_style_show_cursor(Context*);
void                     nk_style_hide_cursor(Context*);

/* Style: stack */
int                      nk_style_push_font(Context*, UserFont*);
int                      nk_style_push_float(Context*, float*, float);
int                      nk_style_push_Vec2(Context*, Vec2*, Vec2);
int                      nk_style_push_style_item(Context*, StyleItem*, StyleItem);
int                      nk_style_push_flags(Context*, nk_flags*, nk_flags);
int                      nk_style_push_color(Context*, Color*, Color);

int                      nk_style_pop_font(Context*);
int                      nk_style_pop_float(Context*);
int                      nk_style_pop_Vec2(Context*);
int                      nk_style_pop_style_item(Context*);
int                      nk_style_pop_flags(Context*);
int                      nk_style_pop_color(Context*);

/* Utilities */
Rect           nk_widget_bounds(Context*);
Vec2           nk_widget_position(Context*);
Vec2           nk_widget_size(Context*);
float                    nk_widget_width(Context*);
float                    nk_widget_height(Context*);
int                      nk_widget_is_hovered(Context*);
int                      nk_widget_is_mouse_clicked(Context*, Buttons);
int                      nk_widget_has_mouse_click_down(Context*, Buttons, int down);
void                     nk_spacing(Context*, int cols);

/* base widget function  */
WidgetLayoutStates nk_widget(Rect*, const(Context)*);
WidgetLayoutStates nk_widget_fitting(Rect*, Context*, Vec2);

/* color (conversion user --> nuklear) */
Color          nk_rgb(int r, int g, int b);
Color          nk_rgb_iv(const(int)* rgb);
Color          nk_rgb_bv(const nk_byte* rgb);
Color          nk_rgb_f(float r, float g, float b);
Color          nk_rgb_fv(const(float)* rgb);
Color          nk_rgb_hex(const(char)* rgb);

Color          nk_rgba(int r, int g, int b, int a);
Color          nk_rgba_u32(nk_uint);
Color          nk_rgba_iv(const(int)* rgba);
Color          nk_rgba_bv(const(nk_byte)* rgba);
Color          nk_rgba_f(float r, float g, float b, float a);
Color          nk_rgba_fv(const(float)* rgba);
Color          nk_rgba_hex(const(char)* rgb);

Color          nk_hsv(int h, int s, int v);
Color          nk_hsv_iv(const(int)* hsv);
Color          nk_hsv_bv(const(nk_byte)* hsv);
Color          nk_hsv_f(float h, float s, float v);
Color          nk_hsv_fv(const(float)* hsv);

Color          nk_hsva(int h, int s, int v, int a);
Color          nk_hsva_iv(const(int)* hsva);
Color          nk_hsva_bv(const(nk_byte)* hsva);
Color          nk_hsva_f(float h, float s, float v, float a);
Color          nk_hsva_fv(const(float)* hsva);

/* color (conversion nuklear --> user) */
void                     nk_color_f(float *r, float *g, float *b, float *a, Color);
void                     nk_color_fv(float *rgba_out, Color);
void                     nk_color_d(double *r, double *g, double *b, double *a, Color);
void                     nk_color_dv(double *rgba_out, Color);

nk_uint                  nk_color_u32(Color);
void                     nk_color_hex_rgba(char *output, Color);
void                     nk_color_hex_rgb(char *output, Color);

void                     nk_color_hsv_i(int *out_h, int *out_s, int *out_v, Color);
void                     nk_color_hsv_b(nk_byte *out_h, nk_byte *out_s, nk_byte *out_v, Color);
void                     nk_color_hsv_iv(int *hsv_out, Color);
void                     nk_color_hsv_bv(nk_byte *hsv_out, Color);
void                     nk_color_hsv_f(float *out_h, float *out_s, float *out_v, Color);
void                     nk_color_hsv_fv(float *hsv_out, Color);

void                     nk_color_hsva_i(int *h, int *s, int *v, int *a, Color);
void                     nk_color_hsva_b(nk_byte *h, nk_byte *s, nk_byte *v, nk_byte *a, Color);
void                     nk_color_hsva_iv(int *hsva_out, Color);
void                     nk_color_hsva_bv(nk_byte *hsva_out, Color);
void                     nk_color_hsva_f(float *out_h, float *out_s, float *out_v, float *out_a, Color);
void                     nk_color_hsva_fv(float *hsva_out, Color);

/* image */
Handle                Handle_ptr(void*);
Handle                Handle_id(int);
Image          nk_image_handle(Handle);
Image          nk_image_ptr(void*);
Image          nk_image_id(int);
int                      nk_image_is_subimage(const Image* img);
Image          nk_subimage_ptr(void*, ushort w, ushort h, Rect sub_region);
Image          nk_subimage_id(int, ushort w, ushort h, Rect sub_region);
Image          nk_subimage_handle(Handle, ushort w, ushort h, Rect sub_region);

/* math */
nk_hash                  nk_murmur_hash(const(void)* key, int len, nk_hash seed);
void                     nk_triangle_from_direction(Vec2 *result, Rect r, float pad_x, float pad_y, Heading);

Vec2           nk_Vec2(float x, float y);
Vec2           nk_Vec2i(int x, int y);
Vec2           nk_Vec2v(const(float)* xy);
Vec2           nk_Vec2iv(const(int)* xy);

Rect           nk_get_null_rect();
Rect           nk_rect(float x, float y, float w, float h);
Rect           nk_recti(int x, int y, int w, int h);
Rect           nk_recta(Vec2 pos, Vec2 size);
Rect           nk_rectv(const(float)* xywh);
Rect           nk_rectiv(const(int)* xywh);
Vec2           nk_rect_pos(Rect);
Vec2           nk_rect_size(Rect);

/* string*/
int                      nk_strlen(const(char)* str);
int                      nk_stricmp(const(char)* s1, const(char)* s2);
int                      nk_stricmpn(const(char)* s1, const(char)* s2, int n);
int                      nk_strtoi(const(char)* str, char **endptr);
float                    nk_strtof(const(char)* str, char **endptr);
double                   nk_strtod(const(char)* str, char **endptr);
int                      nk_strfilter(const(char)* text, const(char)* regexp);
int                      nk_strmatch_fuzzy_string(const(char*)  str, const(char*)  pattern, int *out_score);
int                      nk_strmatch_fuzzy_text(const(char)* txt, int txt_len, const(char)* pattern, int *out_score);

/* UTF-8 */
int                      nk_utf_decode(const(char)*, nk_rune*, int);
int                      nk_utf_encode(nk_rune, char*, int);
int                      nk_utf_len(const(char)*, int byte_len);
const(char)*              nk_utf_at(const(char)* buffer, int length, int index, nk_rune *unicode, int *len);

/* ==============================================================
 *
 *                          MEMORY BUFFER
 *
 * ===============================================================*/
/*  A basic (double)-buffer with linear allocation and resetting as only
    freeing policy. The buffer's main purpose is to control all memory management
    inside the GUI toolkit and still leave memory control as much as possible in
    the hand of the user while also making sure the library is easy to use if
    not as much control is needed.
    In general all memory inside this library can be provided from the user in
    three different ways.

    The first way and the one providing most control is by just passing a fixed
    size memory block. In this case all control lies in the hand of the user
    since he can exactly control where the memory comes from and how much memory
    the library should consume. Of course using the fixed size API removes the
    ability to automatically resize a buffer if not enough memory is provided so
    you have to take over the resizing. While being a fixed sized buffer sounds
    quite limiting, it is very effective in this library since the actual memory
    consumption is quite stable and has a fixed upper bound for a lot of cases.

    If you don't want to think about how much memory the library should allocate
    at all time or have a very dynamic UI with unpredictable memory consumption
    habits but still want control over memory allocation you can use the dynamic
    allocator based API. The allocator consists of two callbacks for allocating
    and freeing memory and optional userdata so you can plugin your own allocator.

    The final and easiest way can be used by defining
    NK_INCLUDE_DEFAULT_ALLOCATOR which uses the standard library memory
    allocation functions malloc and free and takes over complete control over
    memory in this library.
*/
struct MemoryStatus {
    void *memory;
    uint type;
    nk_size size;
    nk_size allocated;
    nk_size needed;
    nk_size calls;
};

enum AllocationType {
    NK_BUFFER_FIXED,
    NK_BUFFER_DYNAMIC
};

enum BufferAllocationType {
    NK_BUFFER_FRONT,
    NK_BUFFER_BACK,
    NK_BUFFER_MAX
};

struct BufferMarker {
    int active;
    nk_size offset;
};

struct Memory {void *ptr;nk_size size;};
struct Buffer {
    BufferMarker[BufferAllocationType.NK_BUFFER_MAX] marker;
    /* buffer marker to free a buffer to a certain offset */
    Allocator pool;
    /* allocator callback for dynamic buffers */
    AllocationType type;
    /* memory management type */
    Memory memory;
    /* memory and size of the current memory block */
    float grow_factor;
    /* growing factor for dynamic memory management */
    nk_size allocated;
    /* total amount of memory allocated */
    nk_size needed;
    /* totally consumed memory given that enough memory is present */
    nk_size calls;
    /* number of allocation calls */
    nk_size size;
    /* current size of the buffer */
};

void nk_buffer_init_default(Buffer*);
void nk_buffer_init(Buffer*, const Allocator*, nk_size size);
void nk_buffer_init_fixed(Buffer*, void *memory, nk_size size);
void nk_buffer_info(MemoryStatus*, Buffer*);
void nk_buffer_push(Buffer*, BufferAllocationType type, const(void)* memory, nk_size size, nk_size align_);
void nk_buffer_mark(Buffer*, BufferAllocationType type);
void nk_buffer_reset(Buffer*, BufferAllocationType type);
void nk_buffer_clear(Buffer*);
void nk_buffer_free(Buffer*);
void *nk_buffer_memory(Buffer*);
const(void)* nk_buffer_memory_const(const(Buffer)*);
nk_size nk_buffer_total(Buffer*);

/* ==============================================================
 *
 *                          STRING
 *
 * ===============================================================*/
/*  Basic string buffer which is only used in context with the text editor
 *  to manage and manipulate dynamic or fixed size string content. This is _NOT_
 *  the default string handling method.*/
struct String {
    Buffer buffer;
    int len; /* in codepoints/runes/glyphs */
};

void nk_str_init_default(String*);
void nk_str_init(String*, const Allocator*, nk_size size);
void nk_str_init_fixed(String*, void *memory, nk_size size);
void nk_str_clear(String*);
void nk_str_free(String*);

int nk_str_append_text_char(String*, const(char)*, int);
int nk_str_append_str_char(String*, const(char)*);
int nk_str_append_text_utf8(String*, const(char)*, int);
int nk_str_append_str_utf8(String*, const(char)*);
int nk_str_append_text_runes(String*, const nk_rune*, int);
int nk_str_append_str_runes(String*, const nk_rune*);

int nk_str_insert_at_char(String*, int pos, const(char)*, int);
int nk_str_insert_at_rune(String*, int pos, const(char)*, int);

int nk_str_insert_text_char(String*, int pos, const(char)*, int);
int nk_str_insert_str_char(String*, int pos, const(char)*);
int nk_str_insert_text_utf8(String*, int pos, const(char)*, int);
int nk_str_insert_str_utf8(String*, int pos, const(char)*);
int nk_str_insert_text_runes(String*, int pos, const nk_rune*, int);
int nk_str_insert_str_runes(String*, int pos, const nk_rune*);

void nk_str_remove_chars(String*, int len);
void nk_str_remove_runes(String *str, int len);
void nk_str_delete_chars(String*, int pos, int len);
void nk_str_delete_runes(String*, int pos, int len);

char *nk_str_at_char(String*, int pos);
char *nk_str_at_rune(String*, int pos, nk_rune *unicode, int *len);
nk_rune nk_str_rune_at(const String*, int pos);
const(char)* nk_str_at_char_const(const String*, int pos);
const(char)* nk_str_at_const(const String*, int pos, nk_rune *unicode, int *len);

char *nk_str_get(String*);
const(char)* nk_str_get_const(const String*);
int nk_str_len(String*);
int nk_str_len_char(String*);

/*===============================================================
 *
 *                      TEXT EDITOR
 *
 * ===============================================================*/
/* Editing text in this library is handled by either `nk_edit_string` or
 * `nk_edit_buffer`. But like almost everything in this library there are multiple
 * ways of doing it and a balance between control and ease of use with memory
 * as well as functionality controlled by flags.
 *
 * This library generally allows three different levels of memory control:
 * First of is the most basic way of just providing a simple char array with
 * string length. This method is probably the easiest way of handling simple
 * user text input. Main upside is complete control over memory while the biggest
 * downside in comparsion with the other two approaches is missing undo/redo.
 *
 * For UIs that require undo/redo the second way was created. It is based on
 * a fixed size nk_text_edit struct, which has an internal undo/redo stack.
 * This is mainly useful if you want something more like a text editor but don't want
 * to have a dynamically growing buffer.
 *
 * The final way is using a dynamically growing nk_text_edit struct, which
 * has both a default version if you don't care where memory comes from and an
 * allocator version if you do. While the text editor is quite powerful for its
 * complexity I would not recommend editing gigabytes of data with it.
 * It is rather designed for uses cases which make sense for a GUI library not for
 * an full blown text editor.
 */

enum NK_TEXTEDIT_UNDOSTATECOUNT    = 99;
enum NK_TEXTEDIT_UNDOCHARCOUNT     = 999;

struct Clipboard {
    Handle userdata;
    nk_plugin_paste paste;
    nk_plugin_copy copy;
};

struct TextUndoRecord {
   int where;
   short insert_length;
   short delete_length;
   short char_storage;
};

struct TextUndoState {
   TextUndoRecord[NK_TEXTEDIT_UNDOSTATECOUNT] undo_rec;
   nk_rune[NK_TEXTEDIT_UNDOCHARCOUNT] undo_char;
   short undo_point;
   short redo_point;
   short undo_char_point;
   short redo_char_point;
};

enum TextEditType {
    NK_TEXT_EDIT_SINGLE_LINE,
    NK_TEXT_EDIT_MULTI_LINE
};

enum TextEditMode {
    NK_TEXT_EDIT_MODE_VIEW,
    NK_TEXT_EDIT_MODE_INSERT,
    NK_TEXT_EDIT_MODE_REPLACE
};

struct TextEdit {
    Clipboard clip;
    String string;
    nk_plugin_filter filter;
    Vec2 scrollbar;

    int cursor;
    int select_start;
    int select_end;
    ubyte mode;
    ubyte cursor_at_end_of_line;
    ubyte initialized;
    ubyte has_preferred_x;
    ubyte single_line;
    ubyte active;
    ubyte padding1;
    float preferred_x;
    TextUndoState undo;
};

/* filter function */
int nk_filter_default(const TextEdit*, nk_rune unicode);
int nk_filter_ascii(const TextEdit*, nk_rune unicode);
int nk_filter_float(const TextEdit*, nk_rune unicode);
int nk_filter_decimal(const TextEdit*, nk_rune unicode);
int nk_filter_hex(const TextEdit*, nk_rune unicode);
int nk_filter_oct(const TextEdit*, nk_rune unicode);
int nk_filter_binary(const TextEdit*, nk_rune unicode);

/* text editor */
void nk_textedit_init_default(TextEdit*);
void nk_textedit_init(TextEdit*, Allocator*, nk_size size);
void nk_textedit_init_fixed(TextEdit*, void *memory, nk_size size);
void nk_textedit_free(TextEdit*);
void nk_textedit_text(TextEdit*, const(char)*, int total_len);
void nk_textedit_delete(TextEdit*, int where, int len);
void nk_textedit_delete_selection(TextEdit*);
void nk_textedit_select_all(TextEdit*);
int nk_textedit_cut(TextEdit*);
int nk_textedit_paste(TextEdit*, const(char*), int len);
void nk_textedit_undo(TextEdit*);
void nk_textedit_redo(TextEdit*);

/* ===============================================================
 *
 *                          FONT
 *
 * ===============================================================*/
/*  Font handling in this library was designed to be quite customizable and lets
    you decide what you want to use and what you want to provide. In this sense
    there are four different degrees between control and ease of use and two
    different drawing APIs to provide for.

    So first of the easiest way to do font handling is by just providing a
    `nk_user_font` struct which only requires the height in pixel of the used
    font and a callback to calculate the width of a string. This way of handling
    fonts is best fitted for using the normal draw shape command API were you
    do all the text drawing yourself and the library does not require any kind
    of deeper knowledge about which font handling mechanism you use.

    While the first approach works fine if you don't want to use the optional
    vertex buffer output it is not enough if you do. To get font handling working
    for these cases you have to provide two additional parameters inside the
    `nk_user_font`. First a texture atlas handle used to draw text as subimages
    of a bigger font atlas texture and a callback to query a character's glyph
    information (offset, size, ...). So it is still possible to provide your own
    font and use the vertex buffer output.

    The final approach if you do not have a font handling functionality or don't
    want to use it in this library is by using the optional font baker. This API
    is divided into a high- and low-level API with different priorities between
    ease of use and control. Both API's can be used to create a font and 
    font atlas texture and can even be used with or without the vertex buffer
    output. So it still uses the `nk_user_font` struct and the two different
    approaches previously stated still work.
    Now to the difference between the low level API and the high level API. The low
    level API provides a lot of control over the baking process of the font and
    provides total control over memory. It consists of a number of functions that
    need to be called from begin to end and each step requires some additional
    configuration, so it is a lot more complex than the high-level API.
    If you don't want to do all the work required for using the low-level API
    you can use the font atlas API. It provides the same functionality as the
    low-level API but takes away some configuration and all of memory control and
    in term provides a easier to use API.
*/

alias nk_text_width_f = float function(Handle, float h, const(char)*, int len);
alias nk_query_font_glyph_f = void function(Handle handle, float font_height,
									UserFontGlyph *glyph,
									nk_rune codepoint, nk_rune next_codepoint);

struct UserFontGlyph {
    Vec2[2] uv;
    /* texture coordinates */
    Vec2 offset;
    /* offset between top left and glyph */
    float width, height;
    /* size of the glyph  */
    float xadvance;
    /* offset to the next glyph */
};

struct UserFont {
    Handle userdata;
    /* user provided font handle */
    float height;
    /* max height of the font */
    nk_text_width_f width;
    /* font string width in pixel callback */
    nk_query_font_glyph_f query;
    /* font glyph callback to query drawing info */
    Handle texture;
    /* texture handle to the used font atlas or texture */
};

enum FontCoordType {
    NK_COORD_UV, /* texture coordinates inside font glyphs are clamped between 0-1 */
    NK_COORD_PIXEL /* texture coordinates inside font glyphs are in absolute pixel */
};

struct BakedFont {
    float height;
    /* height of the font  */
    float ascent, descent;
    /* font glyphs ascent and descent  */
    nk_rune glyph_offset;
    /* glyph array offset inside the font glyph baking output array  */
    nk_rune glyph_count;
    /* number of glyphs of this font inside the glyph baking array output */
    const(nk_rune)* ranges;
    /* font codepoint ranges as pairs of (from/to) and 0 as last element */
};

struct FontConfig {
    FontConfig *next;
    /* NOTE: only used internally */
    void *ttf_blob;
    /* pointer to loaded TTF file memory block.
     * NOTE: not needed for nk_font_atlas_add_from_memory and nk_font_atlas_add_from_file. */
    nk_size ttf_size;
    /* size of the loaded TTF file memory block
     * NOTE: not needed for nk_font_atlas_add_from_memory and nk_font_atlas_add_from_file. */

    ubyte ttf_data_owned_by_atlas;
    /* used inside font atlas: default to: 0*/
    ubyte merge_mode;
    /* merges this font into the last font */
    ubyte pixel_snap;
    /* align every character to pixel boundary (if true set oversample (1,1)) */
    ubyte oversample_v, oversample_h;
    /* rasterize at hight quality for sub-pixel position */
    ubyte[3] padding;

    float size;
    /* baked pixel height of the font */
    FontCoordType coord_type;
    /* texture coordinate format with either pixel or UV coordinates */
    Vec2 spacing;
    /* extra pixel spacing between glyphs  */
    const(nk_rune)* range;
    /* list of unicode ranges (2 values per range, zero terminated) */
    BakedFont *font;
    /* font to setup in the baking process: NOTE: not needed for font atlas */
    nk_rune fallback_glyph;
    /* fallback glyph to use if a given rune is not found */
};

struct FontGlyph {
    nk_rune codepoint;
    float xadvance;
    float x0, y0, x1, y1, w, h;
    float u0, v0, u1, v1;
};

struct Font {
    Font *next;
    UserFont handle;
    BakedFont info;
    float scale;
    FontGlyph *glyphs;
    const(FontGlyph)* fallback;
    nk_rune fallback_codepoint;
    Handle texture;
    FontConfig *config;
};

enum FontAtlasFormat {
    NK_FONT_ATLAS_ALPHA8,
    NK_FONT_ATLAS_RGBA32
};

struct FontAtlas {
    void *pixel;
    int tex_width;
    int tex_height;

    Allocator permanent;
    Allocator temporary;
    Recti custom;
    Cursor[StyleCursor.NK_CURSOR_COUNT] cursors;

    int glyph_count;
    FontGlyph *glyphs;
    Font *default_font;
    Font *fonts;
    FontConfig *config;
    int font_num;
};

/* some language glyph codepoint ranges */
const(nk_rune)* nk_font_default_glyph_ranges();
const(nk_rune)* nk_font_chinese_glyph_ranges();
const(nk_rune)* nk_font_cyrillic_glyph_ranges();
const(nk_rune)* nk_font_korean_glyph_ranges();

void nk_font_atlas_init_default(FontAtlas*);
void nk_font_atlas_init(FontAtlas*, Allocator*);
void nk_font_atlas_init_custom(FontAtlas*, Allocator *persistent, Allocator *transient);
void nk_font_atlas_begin(FontAtlas*);
FontConfig nk_font_config(float pixel_height);
Font *nk_font_atlas_add(FontAtlas*, const(FontConfig)*);
Font* nk_font_atlas_add_default(FontAtlas*, float height, const(FontConfig)*);
Font* nk_font_atlas_add_from_memory(FontAtlas *atlas, void *memory, nk_size size, float height, const(FontConfig)* config);
Font* nk_font_atlas_add_from_file(FontAtlas *atlas, const(char)* file_path, float height, const(FontConfig)*);
Font *nk_font_atlas_add_compressed(FontAtlas*, void *memory, nk_size size, float height, const(FontConfig)*);
Font* nk_font_atlas_add_compressed_base85(FontAtlas*, const(char)* data, float height, const(FontConfig)* config);
const(void)* nk_font_atlas_bake(FontAtlas*, int *width, int *height, FontAtlasFormat);
void nk_font_atlas_end(FontAtlas*, Handle tex, DrawNullTexture*);
void nk_font_atlas_clear(FontAtlas*);
const(FontGlyph)* nk_font_find_glyph(Font*, nk_rune unicode);

/* ===============================================================
 *
 *                          DRAWING
 *
 * ===============================================================*/
/*  This library was designed to be render backend agnostic so it does
    not draw anything to screen. Instead all drawn shapes, widgets
    are made of, are buffered into memory and make up a command queue.
    Each frame therefore fills the command buffer with draw commands
    that then need to be executed by the user and his own render backend.
    After that the command buffer needs to be cleared and a new frame can be
    started. It is probably important to note that the command buffer is the main
    drawing API and the optional vertex buffer API only takes this format and
    converts it into a hardware accessible format.

    Draw commands are divided into filled shapes and shape outlines but only
    filled shapes as well as line, curves and scissor are required to be provided.
    All other shape drawing commands can be used but are not required. This was
    done to allow the maximum number of render backends to be able to use this
    library without you having to do additional work.
*/
enum CommandType {
    NK_COMMAND_NOP,
    NK_COMMAND_SCISSOR,
    NK_COMMAND_LINE,
    NK_COMMAND_CURVE,
    NK_COMMAND_RECT,
    NK_COMMAND_RECT_FILLED,
    NK_COMMAND_RECT_MULTI_COLOR,
    NK_COMMAND_CIRCLE,
    NK_COMMAND_CIRCLE_FILLED,
    NK_COMMAND_ARC,
    NK_COMMAND_ARC_FILLED,
    NK_COMMAND_TRIANGLE,
    NK_COMMAND_TRIANGLE_FILLED,
    NK_COMMAND_POLYGON,
    NK_COMMAND_POLYGON_FILLED,
    NK_COMMAND_POLYLINE,
    NK_COMMAND_TEXT,
    NK_COMMAND_IMAGE
};

/* command base and header of every command inside the buffer */
struct Command {
    CommandType type;
    nk_size next;
    Handle userdata;
};

struct CommandScissor {
    Command header;
    short x, y;
    ushort w, h;
};

struct CommandLine {
    Command header;
    ushort line_thickness;
    Vec2i begin;
    Vec2i end;
    Color color;
};

struct CommandCurve {
    Command header;
    ushort line_thickness;
    Vec2i begin;
    Vec2i end;
    Vec2i[2] ctrl;
    Color color;
};

struct CommandRect {
    Command header;
    ushort rounding;
    ushort line_thickness;
    short x, y;
    ushort w, h;
    Color color;
};

struct CommandRectFilled {
    Command header;
    ushort rounding;
    short x, y;
    ushort w, h;
    Color color;
};

struct CommandRectMultiColor {
    Command header;
    short x, y;
    ushort w, h;
    Color left;
    Color top;
    Color bottom;
    Color right;
};

struct CommandTriangle {
    Command header;
    ushort line_thickness;
    Vec2i a;
    Vec2i b;
    Vec2i c;
    Color color;
};

struct CommandTriangle_filled {
    Command header;
    Vec2i a;
    Vec2i b;
    Vec2i c;
    Color color;
};

struct CommandCircle {
    Command header;
    short x, y;
    ushort line_thickness;
    ushort w, h;
    Color color;
};

struct CommandCircleFilled {
    Command header;
    short x, y;
    ushort w, h;
    Color color;
};

struct CommandArc {
    Command header;
    short cx, cy;
    ushort r;
    ushort line_thickness;
    float[2] a;
    Color color;
};

struct CommandArcFilled {
    Command header;
    short cx, cy;
    ushort r;
    float[2] a;
    Color color;
};

struct CommandPolygon {
    Command header;
    Color color;
    ushort line_thickness;
    ushort point_count;
    Vec2i[1] points;
};

struct CommandPolygonFilled {
    Command header;
    Color color;
    ushort point_count;
    Vec2i[1] points;
};

struct CommandPolyline {
    Command header;
    Color color;
    ushort line_thickness;
    ushort point_count;
    Vec2i[1] points;
};

struct CommandImage {
    Command header;
    short x, y;
    ushort w, h;
    Image img;
    Color col;
};

struct CommandText {
    Command header;
    const(UserFont)* font;
    Color background;
    Color foreground;
    short x, y;
    ushort w, h;
    float height;
    int length;
    char[1] string;
};

enum CommandClipping {
    NK_CLIPPING_OFF = nk_false,
    NK_CLIPPING_ON = nk_true
};

struct CommandBuffer {
    Buffer *base;
    Rect clip;
    int use_clipping;
    Handle userdata;
    nk_size begin, end, last;
};

/* shape outlines */
void nk_stroke_line(CommandBuffer *b, float x0, float y0, float x1, float y1, float line_thickness, Color);
void nk_stroke_curve(CommandBuffer*, float, float, float, float, float, float, float, float, float line_thickness, Color);
void nk_stroke_rect(CommandBuffer*, Rect, float rounding, float line_thickness, Color);
void nk_stroke_circle(CommandBuffer*, Rect, float line_thickness, Color);
void nk_stroke_arc(CommandBuffer*, float cx, float cy, float radius, float a_min, float a_max, float line_thickness, Color);
void nk_stroke_triangle(CommandBuffer*, float, float, float, float, float, float, float line_thichness, Color);
void nk_stroke_polyline(CommandBuffer*, float *points, int point_count, float line_thickness, Color col);
void nk_stroke_polygon(CommandBuffer*, float*, int point_count, float line_thickness, Color);

/* filled shades */
void nk_fill_rect(CommandBuffer*, Rect, float rounding, Color);
void nk_fill_rect_multi_color(CommandBuffer*, Rect, Color left, Color top, Color right, Color bottom);
void nk_fill_circle(CommandBuffer*, Rect, Color);
void nk_fill_arc(CommandBuffer*, float cx, float cy, float radius, float a_min, float a_max, Color);
void nk_fill_triangle(CommandBuffer*, float x0, float y0, float x1, float y1, float x2, float y2, Color);
void nk_fill_polygon(CommandBuffer*, float*, int point_count, Color);

/* misc */
void nk_push_scissor(CommandBuffer*, Rect);
void nk_draw_image(CommandBuffer*, Rect, const Image*, Color);
void nk_draw_text(CommandBuffer*, Rect, const(char)* text, int len, const(UserFont)*, Color, Color);
const(Command)* nk__next(Context*, const(Command)*);
const(Command)* nk__begin(Context*);

/* ===============================================================
 *
 *                          INPUT
 *
 * ===============================================================*/
struct MouseButton {
    int down;
    uint clicked;
    Vec2 clicked_pos;
};

struct Mouse {
    MouseButton[Buttons.NK_BUTTON_MAX] buttons;
    Vec2 pos;
    Vec2 prev;
    Vec2 delta;
    float scroll_delta;
    ubyte grab;
    ubyte grabbed;
    ubyte ungrab;
};

struct Key {
    int down;
    uint clicked;
};

struct Keyboard {
    Key[Keys.NK_KEY_MAX] keys;
    char[NK_INPUT_MAX] text;
    int text_len;
};

struct Input {
    Keyboard keyboard;
    Mouse mouse;
};

int nk_input_has_mouse_click(const Input*, Buttons);
int nk_input_has_mouse_click_in_rect(const Input*, Buttons, Rect);
int nk_input_has_mouse_click_down_in_rect(const Input*, Buttons, Rect, int down);
int nk_input_is_mouse_click_in_rect(const Input*, Buttons, Rect);
int nk_input_is_mouse_click_down_in_rect(const Input *i, Buttons id, Rect b, int down);
int nk_input_any_mouse_click_in_rect(const Input*, Rect);
int nk_input_is_mouse_prev_hovering_rect(const Input*, Rect);
int nk_input_is_mouse_hovering_rect(const Input*, Rect);
int nk_input_mouse_clicked(const Input*, Buttons, Rect);
int nk_input_is_mouse_down(const Input*, Buttons);
int nk_input_is_mouse_pressed(const Input*, Buttons);
int nk_input_is_mouse_released(const Input*, Buttons);
int nk_input_is_key_pressed(const Input*, Keys);
int nk_input_is_key_released(const Input*, Keys);
int nk_input_is_key_down(const Input*, Keys);


/* ===============================================================
 *
 *                          DRAW LIST
 *
 * ===============================================================*/

/*  The optional vertex buffer draw list provides a 2D drawing context
    with antialiasing functionality which takes basic filled or outlined shapes
    or a path and outputs vertexes, elements and draw commands.
    The actual draw list API is not required to be used directly while using this
    library since converting the default library draw command output is done by
    just calling `nk_convert` but I decided to still make this library accessible
    since it can be useful.

    The draw list is based on a path buffering and polygon and polyline
    rendering API which allows a lot of ways to draw 2D content to screen.
    In fact it is probably more powerful than needed but allows even more crazy
    things than this library provides by default.
*/
alias nk_draw_index = nk_ushort;
enum DrawListStroke {
    NK_STROKE_OPEN = nk_false,
    /* build up path has no connection back to the beginning */
    NK_STROKE_CLOSED = nk_true
    /* build up path has a connection back to the beginning */
};

enum DrawVertexLayoutAttribute {
    NK_VERTEX_POSITION,
    NK_VERTEX_COLOR,
    NK_VERTEX_TEXCOORD,
    NK_VERTEX_ATTRIBUTE_COUNT
};

enum DrawVertexLayoutFormat {
    NK_FORMAT_SCHAR,
    NK_FORMAT_SSHORT,
    NK_FORMAT_SINT,
    NK_FORMAT_UCHAR,
    NK_FORMAT_USHORT,
    NK_FORMAT_UINT,
    NK_FORMAT_FLOAT,
    NK_FORMAT_DOUBLE,

NK_FORMAT_COLOR_BEGIN,
    NK_FORMAT_R8G8B8 = NK_FORMAT_COLOR_BEGIN,
    NK_FORMAT_R16G15B16,
    NK_FORMAT_R32G32B32,

    NK_FORMAT_R8G8B8A8,
    NK_FORMAT_R16G15B16A16,
    NK_FORMAT_R32G32B32A32,
    NK_FORMAT_R32G32B32A32_FLOAT,
    NK_FORMAT_R32G32B32A32_DOUBLE,

    NK_FORMAT_RGB32,
    NK_FORMAT_RGBA32,
NK_FORMAT_COLOR_END = NK_FORMAT_RGBA32,
    NK_FORMAT_COUNT
};

// enum NK_VERTEX_LAYOUT_END NK_VERTEX_ATTRIBUTE_COUNT,NK_FORMAT_COUNT,0
struct DrawVertexLayoutElement {
    DrawVertexLayoutAttribute attribute;
    DrawVertexLayoutFormat format;
    nk_size offset;
};

struct DrawCommand {
    uint elem_count;
    /* number of elements in the current draw batch */
    Rect clip_rect;
    /* current screen clipping rectangle */
    Handle texture;
    /* current texture to set */
    Handle userdata;
};

struct DrawList {
    ConvertConfig config;
    Rect clip_rect;
    Buffer *buffer;
    Buffer *vertices;
    Buffer *elements;
    uint element_count;
    uint vertex_count;
    nk_size cmd_offset;
    uint cmd_count;
    uint path_count;
    uint path_offset;
    Vec2[12] circle_vtx;
    Handle userdata;
};

/* draw list */
void nk_draw_list_init(DrawList*);
void nk_draw_list_setup(DrawList *canvas, const(ConvertConfig)* config, Buffer *cmds, Buffer *vertices, Buffer *elements);
void nk_draw_list_clear(DrawList*);

/* drawing */
// #define nk_draw_list_foreach(cmd, can, b) for((cmd)=nk__draw_list_begin(can, b); (cmd)!=0; (cmd)=nk__draw_list_next(cmd, b, can))
const(DrawCommand)* nk__draw_list_begin(const(DrawList)*, const(Buffer)*);
const(DrawCommand)* nk__draw_list_next(const(DrawCommand)*, const(Buffer)*, const(DrawList)*);
const(DrawCommand)* nk__draw_list_end(const(DrawList)*, const(Buffer)*);
void nk_draw_list_clear(DrawList *list);

/* path */
void nk_draw_list_path_clear(DrawList*);
void nk_draw_list_path_line_to(DrawList *list, Vec2 pos);
void nk_draw_list_path_arc_to_fast(DrawList*, Vec2 center, float radius, int a_min, int a_max);
void nk_draw_list_path_arc_to(DrawList*, Vec2 center, float radius, float a_min, float a_max, uint segments);
void nk_draw_list_path_rect_to(DrawList*, Vec2 a, Vec2 b, float rounding);
void nk_draw_list_path_curve_to(DrawList*, Vec2 p2, Vec2 p3, Vec2 p4, uint num_segments);
void nk_draw_list_path_fill(DrawList*, Color);
void nk_draw_list_path_stroke(DrawList*, Color, DrawListStroke closed, float thickness);

/* stroke */
void nk_draw_list_stroke_line(DrawList*, Vec2 a, Vec2 b, Color, float thickness);
void nk_draw_list_stroke_rect(DrawList*, Rect rect, Color, float rounding, float thickness);
void nk_draw_list_stroke_triangle(DrawList*, Vec2 a, Vec2 b, Vec2 c, Color, float thickness);
void nk_draw_list_stroke_circle(DrawList*, Vec2 center, float radius, Color, uint segs, float thickness);
void nk_draw_list_stroke_curve(DrawList*, Vec2 p0, Vec2 cp0, Vec2 cp1, Vec2 p1, Color, uint segments, float thickness);
void nk_draw_list_stroke_poly_line(DrawList*, const Vec2 *pnts, const uint cnt, Color, DrawListStroke, float thickness, AntiAliasing);

/* fill */
void nk_draw_list_fill_rect(DrawList*, Rect rect, Color, float rounding);
void nk_draw_list_fill_rect_multi_color(DrawList *list, Rect rect, Color left, Color top, Color right, Color bottom);
void nk_draw_list_fill_triangle(DrawList*, Vec2 a, Vec2 b, Vec2 c, Color);
void nk_draw_list_fill_circle(DrawList*, Vec2 center, float radius, Color col, uint segs);
void nk_draw_list_fill_poly_convex(DrawList*, const Vec2 *points, const uint count, Color, AntiAliasing);

/* misc */
void nk_draw_list_add_image(DrawList*, Image texture, Rect rect, Color);
void nk_draw_list_add_text(DrawList*, const(UserFont)*, Rect, const(char)* text, int len, float font_height, Color);
void nk_draw_list_push_userdata(DrawList*, Handle userdata);

/* ===============================================================
 *
 *                          GUI
 *
 * ===============================================================*/
enum StyleItemType {
    NK_STYLE_ITEM_COLOR,
    NK_STYLE_ITEM_IMAGE
};

union StyleItemData {
    Image image;
    Color color;
};

struct StyleItem {
    StyleItemType type;
    StyleItemData data;
};

struct StyleText {
    Color color;
    Vec2 padding;
};

struct StyleButton {
    /* background */
    StyleItem normal;
    StyleItem hover;
    StyleItem active;
    Color border_color;

    /* text */
    Color text_background;
    Color text_normal;
    Color text_hover;
    Color text_active;
    nk_flags text_alignment;

    /* properties */
    float border;
    float rounding;
    Vec2 padding;
    Vec2 image_padding;
    Vec2 touch_padding;

    /* optional user callbacks */
    Handle userdata;
	void function(CommandBuffer*, Handle userdata) draw_begin;
	void function(CommandBuffer*, Handle userdata) draw_end;
};

struct StyleToggle {
    /* background */
    StyleItem normal;
    StyleItem hover;
    StyleItem active;
    Color border_color;

    /* cursor */
    StyleItem cursor_normal;
    StyleItem cursor_hover;

    /* text */
    Color text_normal;
    Color text_hover;
    Color text_active;
    Color text_background;
    nk_flags text_alignment;

    /* properties */
    Vec2 padding;
    Vec2 touch_padding;
    float spacing;
    float border;

    /* optional user callbacks */
    Handle userdata;
    void function(CommandBuffer*, Handle userdata) draw_begin;
	void function(CommandBuffer*, Handle userdata) draw_end;
};

struct StyleSelectable {
    /* background (inactive) */
    StyleItem normal;
    StyleItem hover;
    StyleItem pressed;

    /* background (active) */
    StyleItem normal_active;
    StyleItem hover_active;
    StyleItem pressed_active;

    /* text color (inactive) */
    Color text_normal;
    Color text_hover;
    Color text_pressed;

    /* text color (active) */
    Color text_normal_active;
    Color text_hover_active;
    Color text_pressed_active;
    Color text_background;
    nk_flags text_alignment;

    /* properties */
    float rounding;
    Vec2 padding;
    Vec2 touch_padding;
    Vec2 image_padding;

    /* optional user callbacks */
    Handle userdata;
    void function(CommandBuffer*, Handle userdata) draw_begin;
	void function(CommandBuffer*, Handle userdata) draw_end;
};

struct StyleSlider {
    /* background */
    StyleItem normal;
    StyleItem hover;
    StyleItem active;
    Color border_color;

    /* background bar */
    Color bar_normal;
    Color bar_hover;
    Color bar_active;
    Color bar_filled;

    /* cursor */
    StyleItem cursor_normal;
    StyleItem cursor_hover;
    StyleItem cursor_active;

    /* properties */
    float border;
    float rounding;
    float bar_height;
    Vec2 padding;
    Vec2 spacing;
    Vec2 cursor_size;

    /* optional buttons */
    int show_buttons;
    StyleButton inc_button;
    StyleButton dec_button;
    SymbolType inc_symbol;
    SymbolType dec_symbol;

    /* optional user callbacks */
    Handle userdata;
    void function(CommandBuffer*, Handle userdata) draw_begin;
	void function(CommandBuffer*, Handle userdata) draw_end;
};

struct StyleProgress {
    /* background */
    StyleItem normal;
    StyleItem hover;
    StyleItem active;
    Color border_color;

    /* cursor */
    StyleItem cursor_normal;
    StyleItem cursor_hover;
    StyleItem cursor_active;
    Color cursor_border_color;

    /* properties */
    float rounding;
    float border;
    float cursor_border;
    float cursor_rounding;
    Vec2 padding;

    /* optional user callbacks */
    Handle userdata;
   	void function(CommandBuffer*, Handle userdata) draw_begin;
	void function(CommandBuffer*, Handle userdata) draw_end;
};

struct StyleScrollbar {
    /* background */
    StyleItem normal;
    StyleItem hover;
    StyleItem active;
    Color border_color;

    /* cursor */
    StyleItem cursor_normal;
    StyleItem cursor_hover;
    StyleItem cursor_active;
    Color cursor_border_color;

    /* properties */
    float border;
    float rounding;
    float border_cursor;
    float rounding_cursor;
    Vec2 padding;

    /* optional buttons */
    int show_buttons;
    StyleButton inc_button;
    StyleButton dec_button;
    SymbolType inc_symbol;
    SymbolType dec_symbol;

    /* optional user callbacks */
    Handle userdata;
    void function(CommandBuffer*, Handle userdata) draw_begin;
	void function(CommandBuffer*, Handle userdata) draw_end;
};

struct StyleEdit {
    /* background */
    StyleItem normal;
    StyleItem hover;
    StyleItem active;
    Color border_color;
    StyleScrollbar scrollbar;

    /* cursor  */
    Color cursor_normal;
    Color cursor_hover;
    Color cursor_text_normal;
    Color cursor_text_hover;

    /* text (unselected) */
    Color text_normal;
    Color text_hover;
    Color text_active;

    /* text (selected) */
    Color selected_normal;
    Color selected_hover;
    Color selected_text_normal;
    Color selected_text_hover;

    /* properties */
    float border;
    float rounding;
    float cursor_size;
    Vec2 scrollbar_size;
    Vec2 padding;
    float row_padding;
};

struct StyleProperty {
    /* background */
    StyleItem normal;
    StyleItem hover;
    StyleItem active;
    Color border_color;

    /* text */
    Color label_normal;
    Color label_hover;
    Color label_active;

    /* symbols */
    SymbolType sym_left;
    SymbolType sym_right;

    /* properties */
    float border;
    float rounding;
    Vec2 padding;

    StyleEdit edit;
    StyleButton inc_button;
    StyleButton dec_button;

    /* optional user callbacks */
    Handle userdata;
    void function(CommandBuffer*, Handle userdata) draw_begin;
	void function(CommandBuffer*, Handle userdata) draw_end;
};

struct StyleChart {
    /* colors */
    StyleItem background;
    Color border_color;
    Color selected_color;
    Color color;

    /* properties */
    float border;
    float rounding;
    Vec2 padding;
};

struct StyleCombo {
    /* background */
    StyleItem normal;
    StyleItem hover;
    StyleItem active;
    Color border_color;

    /* label */
    Color label_normal;
    Color label_hover;
    Color label_active;

    /* symbol */
    Color symbol_normal;
    Color symbol_hover;
    Color symbol_active;

    /* button */
    StyleButton button;
    SymbolType sym_normal;
    SymbolType sym_hover;
    SymbolType sym_active;

    /* properties */
    float border;
    float rounding;
    Vec2 content_padding;
    Vec2 button_padding;
    Vec2 spacing;
};

struct StyleTab {
    /* background */
    StyleItem background;
    Color border_color;
    Color text;

    /* button */
    StyleButton tab_maximize_button;
    StyleButton tab_minimize_button;
    StyleButton node_maximize_button;
    StyleButton node_minimize_button;
    SymbolType sym_minimize;
    SymbolType sym_maximize;

    /* properties */
    float border;
    float rounding;
    float indent;
    Vec2 padding;
    Vec2 spacing;
};

enum StyleHeaderAlign {
    NK_HEADER_LEFT,
    NK_HEADER_RIGHT
};

struct StyleWindowHeader {
    /* background */
    StyleItem normal;
    StyleItem hover;
    StyleItem active;

    /* button */
    StyleButton close_button;
    StyleButton minimize_button;
    SymbolType close_symbol;
    SymbolType minimize_symbol;
    SymbolType maximize_symbol;

    /* title */
    Color label_normal;
    Color label_hover;
    Color label_active;

    /* properties */
    StyleHeaderAlign align_;
    Vec2 padding;
    Vec2 label_padding;
    Vec2 spacing;
};

struct StyleWindow {
    StyleWindowHeader header;
    StyleItem fixed_background;
    Color background;

    Color border_color;
    Color popup_border_color;
    Color combo_border_color;
    Color contextual_border_color;
    Color menu_border_color;
    Color group_border_color;
    Color tooltip_border_color;
    StyleItem scaler;

    float border;
    float combo_border;
    float contextual_border;
    float menu_border;
    float group_border;
    float tooltip_border;
    float popup_border;

    float rounding;
    Vec2 spacing;
    Vec2 scrollbar_size;
    Vec2 min_size;

    Vec2 padding;
    Vec2 group_padding;
    Vec2 popup_padding;
    Vec2 combo_padding;
    Vec2 contextual_padding;
    Vec2 menu_padding;
    Vec2 tooltip_padding;
};

struct Style {
    const(UserFont)* font;
    const(Cursor)*[StyleCursor.NK_CURSOR_COUNT] cursors;
    const(Cursor)* cursor_active;
    Cursor *cursor_last;
    int cursor_visible;

    StyleText text;
    StyleButton button;
    StyleButton contextual_button;
    StyleButton menu_button;
    StyleToggle option;
    StyleToggle checkbox;
    StyleSelectable selectable;
    StyleSlider slider;
    StyleProgress progress;
    StyleProperty property;
    StyleEdit edit;
    StyleChart chart;
    StyleScrollbar scrollh;
    StyleScrollbar scrollv;
    StyleTab tab;
    StyleCombo combo;
    StyleWindow window;
};

StyleItem nk_style_item_image(Image img);
StyleItem nk_style_item_color(Color);
StyleItem nk_style_item_hide();

/*==============================================================
 *                          PANEL
 * =============================================================*/
enum NK_CHART_MAX_SLOT = 4;

enum PanelType {
    NK_PANEL_WINDOW     = NK_FLAG(0),
    NK_PANEL_GROUP      = NK_FLAG(1),
    NK_PANEL_POPUP      = NK_FLAG(2),
    NK_PANEL_CONTEXTUAL = NK_FLAG(4),
    NK_PANEL_COMBO      = NK_FLAG(5),
    NK_PANEL_MENU       = NK_FLAG(6),
    NK_PANEL_TOOLTIP    = NK_FLAG(7)
};
enum PanelSet {
    NK_PANEL_SET_NONBLOCK = PanelType.NK_PANEL_CONTEXTUAL|PanelType.NK_PANEL_COMBO|PanelType.NK_PANEL_MENU|PanelType.NK_PANEL_TOOLTIP,
    NK_PANEL_SET_POPUP = cast(PanelType)NK_PANEL_SET_NONBLOCK|PanelType.NK_PANEL_POPUP,
    NK_PANEL_SET_SUB = cast(PanelType)NK_PANEL_SET_POPUP|PanelType.NK_PANEL_GROUP
};

struct ChartSlot {
    ChartType type;
    Color color;
    Color highlight;
    float min, max, range;
    int count;
    Vec2 last;
    int index;
};

struct Chart {
    ChartSlot[NK_CHART_MAX_SLOT] slots;
    int slot;
    float x, y, w, h;
};

struct RowLayout {
    int type;
    int index;
    float height;
    int columns;
    const(float)* ratio;
    float item_width, item_height;
    float item_offset;
    float filled;
    Rect item;
    int tree_depth;
};

struct PopupBuffer {
    nk_size begin;
    nk_size parent;
    nk_size last;
    nk_size end;
    int active;
};

struct MenuState {
    float x, y, w, h;
    Scroll offset;
};

struct Panel {
    PanelType type;
    nk_flags flags;
    Rect bounds;
    Scroll *offset;
    float at_x, at_y, max_x;
    float footer_height;
    float header_height;
    float border;
    uint has_scrolling;
    Rect clip;
    MenuState menu;
    RowLayout row;
    Chart chart;
    PopupBuffer popup_buffer;
    CommandBuffer *buffer;
    Panel *parent;
};

/*==============================================================
 *                          WINDOW
 * =============================================================*/
enum NK_WINDOW_MAX_NAME = 64;

enum WindowFlags {
    NK_WINDOW_PRIVATE       = NK_FLAG(10),
    NK_WINDOW_DYNAMIC       = NK_WINDOW_PRIVATE,
    /* special window type growing up in height while being filled to a certain maximum height */
    NK_WINDOW_ROM           = NK_FLAG(11),
    /* sets the window into a read only mode and does not allow input changes */
    NK_WINDOW_HIDDEN        = NK_FLAG(12),
    /* Hides the window and stops any window interaction and drawing */
    NK_WINDOW_CLOSED        = NK_FLAG(13),
    /* Directly closes and frees the window at the end of the frame */
    NK_WINDOW_MINIMIZED     = NK_FLAG(14),
    /* marks the window as minimized */
    NK_WINDOW_REMOVE_ROM    = NK_FLAG(15)
    /* Removes the read only mode at the end of the window */
};

struct PopupState {
    Window *win;
    PanelType type;
    nk_hash name;
    int active;
    uint combo_count;
    uint con_count, con_old;
    uint active_con;
    Rect header;
};

struct EditState {
    nk_hash name;
    uint seq;
    uint old;
    int active, prev;
    int cursor;
    int sel_start;
    int sel_end;
    Scroll scrollbar;
    ubyte mode;
    ubyte single_line;
};

struct PropertyState {
    int active, prev;
    char[NK_MAX_NUMBER_BUFFER] buffer;
    int length;
    int cursor;
    nk_hash name;
    uint seq;
    uint old;
    int state;
};

struct Window {
    uint seq;
    nk_hash name;
    char[NK_WINDOW_MAX_NAME] name_string;
    nk_flags flags;
    Rect bounds;
    Scroll scrollbar;
    CommandBuffer buffer;
    Panel *layout;
    float scrollbar_hiding_timer;

    /* persistent widget state */
    PropertyState property;
    PopupState popup;
    EditState edit;
    uint scrolled;

    Table *tables;
    ushort table_count;
    ushort table_size;

    /* window list hooks */
    Window *next;
    Window *prev;
    Window *parent;
};

/*==============================================================
 *                          STACK
 * =============================================================*/
enum NK_BUTTON_BEHAVIOR_STACK_SIZE = 8;
enum NK_FONT_STACK_SIZE = 8;
enum NK_STYLE_ITEM_STACK_SIZE = 16;
enum NK_FLOAT_STACK_SIZE = 32;
enum NK_VECTOR_STACK_SIZE = 16;
enum NK_FLAGS_STACK_SIZE = 32;
enum NK_COLOR_STACK_SIZE = 32;

struct ConfigurationStack(T, size_t size)
{
	int head;
	ConfigurationStackType!T[size] elements;
}

struct ConfigurationStackType(T)
{
	T *address;
	T old_value;
}

// enum NK_CONFIGURATION_STACK_TYPE(prefix, name, type)\
//     struct nk_config_stack_##name##_element {\
//         prefix##_##type *address;\
//         prefix##_##type old_value;\
//     }
// enum NK_CONFIG_STACK(type,size)\
//     struct nk_config_stack_##type {\
//         int head;\
//         struct nk_config_stack_##type##_element elements[size];\
//     }

alias nk_float = float;
// NK_CONFIGURATION_STACK_TYPE(struct nk, style_item, style_item);
// NK_CONFIGURATION_STACK_TYPE(nk ,float, float);
// NK_CONFIGURATION_STACK_TYPE(struct nk, Vec2, Vec2);
// NK_CONFIGURATION_STACK_TYPE(nk ,flags, flags);
// NK_CONFIGURATION_STACK_TYPE(struct nk, color, color);
// NK_CONFIGURATION_STACK_TYPE(const struct nk, user_font, user_font*);
// NK_CONFIGURATION_STACK_TYPE(enum nk, button_behavior, button_behavior);

// NK_CONFIG_STACK(style_item, NK_STYLE_ITEM_STACK_SIZE);
// NK_CONFIG_STACK(float, NK_FLOAT_STACK_SIZE);
// NK_CONFIG_STACK(Vec2, NK_VECTOR_STACK_SIZE);
// NK_CONFIG_STACK(flags, NK_FLAGS_STACK_SIZE);
// NK_CONFIG_STACK(color, NK_COLOR_STACK_SIZE);
// NK_CONFIG_STACK(user_font, NK_FONT_STACK_SIZE);
// NK_CONFIG_STACK(button_behavior, NK_BUTTON_BEHAVIOR_STACK_SIZE);

struct ConfigurationStacks {
	ConfigurationStack!(StyleItem, NK_STYLE_ITEM_STACK_SIZE) style_items;
	ConfigurationStack!(float, NK_FLOAT_STACK_SIZE) floats;
	ConfigurationStack!(Vec2, NK_VECTOR_STACK_SIZE) vectors;
	ConfigurationStack!(nk_flags, NK_FLAGS_STACK_SIZE) flags;
	ConfigurationStack!(Color, NK_COLOR_STACK_SIZE) colors;
	ConfigurationStack!(UserFont*, NK_FONT_STACK_SIZE) fonts;
	ConfigurationStack!(ButtonBehavior, NK_BUTTON_BEHAVIOR_STACK_SIZE) button_behaviors;
};

/*==============================================================
 *                          CONTEXT
 * =============================================================*/
enum NK_VALUE_PAGE_CAPACITY = ((Window.sizeof / nk_uint.sizeof) / 2);

struct Table {
    uint seq;
    nk_hash[NK_VALUE_PAGE_CAPACITY] keys;
    nk_uint[NK_VALUE_PAGE_CAPACITY] values;
    Table *next;
	Table *prev;
};

union PageData {
    Table tbl;
    Window win;
};

struct PageElement {
    PageData data;
    PageElement *next;
    PageElement *prev;
};

struct Page {
    uint size;
    Page *next;
    PageElement[1] win;
};

struct Pool {
    Allocator alloc;
    AllocationType type;
    uint page_count;
    Page *pages;
    PageElement *freelist;
    uint capacity;
    nk_size size;
    nk_size cap;
};

struct Context {
/* public: can be accessed freely */
    Input input;
    Style style;
    Buffer memory;
    Clipboard clip;
    nk_flags last_widget_state;
    float delta_time_seconds;
    ButtonBehavior button_behavior;
    ConfigurationStacks stacks;

/* private:
    should only be accessed if you
    know what you are doing */
    DrawList draw_list;
    Handle userdata;
    /* text editor objects are quite big because of an internal
     * undo/redo stack. Therefore does not make sense to have one for
     * each window for temporary use cases, so I only provide *one* instance
     * for all windows. This works because the content is cleared anyway */
    TextEdit text_edit;
    /* draw buffer used for overlay drawing operation like cursor */
    CommandBuffer overlay;

    /* windows */
    int build;
    int use_pool;
    Pool pool;
    Window *begin;
    Window *end;
    Window *active;
    Window *current;
    PageElement *freelist;
    uint count;
    uint seq;
};

/* ==============================================================
 *                          MATH
 * =============================================================== */
// enum NK_MIN(a,b) ((a) < (b) ? (a) : (b))
// enum NK_MAX(a,b) ((a) < (b) ? (b) : (a))
// enum NK_CLAMP(i,v,x) (NK_MAX(NK_MIN(v,x), i))

// enum NK_PI 3.141592654f
// enum NK_UTF_INVALID 0xFFFD
// enum NK_MAX_FLOAT_PRECISION 2

// enum NK_UNUSED(x) ((void)(x))
// enum NK_SATURATE(x) (NK_MAX(0, NK_MIN(1.0f, x)))
// enum NK_LEN(a) (sizeof(a)/sizeof(a)[0])
// enum NK_ABS(a) (((a) < 0) ? -(a) : (a))
// enum NK_BETWEEN(x, a, b) ((a) <= (x) && (x) <= (b))
// enum NK_INBOX(px, py, x, y, w, h)\
//     (NK_BETWEEN(px,x,x+w) && NK_BETWEEN(py,y,y+h))
// enum NK_INTERSECT(x0, y0, w0, h0, x1, y1, w1, h1) \
//     (!(((x1 > (x0 + w0)) || ((x1 + w1) < x0) || (y1 > (y0 + h0)) || (y1 + h1) < y0)))
// enum NK_CONTAINS(x, y, w, h, bx, by, bw, bh)\
//     (NK_INBOX(x,y, bx, by, bw, bh) && NK_INBOX(x+w,y+h, bx, by, bw, bh))

// #define nk_Vec2_sub(a, b) nk_Vec2((a).x - (b).x, (a).y - (b).y)
// #define nk_Vec2_add(a, b) nk_Vec2((a).x + (b).x, (a).y + (b).y)
// #define nk_Vec2_len_sqr(a) ((a).x*(a).x+(a).y*(a).y)
// #define nk_Vec2_muls(a, t) nk_Vec2((a).x * (t), (a).y * (t))

// #define nk_ptr_add(t, p, i) ((t*)((void*)((nk_byte*)(p) + (i))))
// #define nk_ptr_add_const(t, p, i) ((const t*)((const(void)*)((const nk_byte*)(p) + (i))))
// #define nk_zero_struct(s) nk_zero(&s, sizeof(s))
